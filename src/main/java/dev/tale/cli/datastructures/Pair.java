package dev.tale.cli.datastructures;

import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
@EqualsAndHashCode
public class Pair<T, U> implements Comparable<Pair<T, U>> {
    private final T left;
    private final U right;

    @Override
    public int compareTo(Pair<T, U> tuPair) {
        if (tuPair == null) {
            return 0;
        }
        int comp = this.left.toString().compareTo(tuPair.left.toString());
        return comp != 0 //
                ? comp //
                : this.right.toString().compareTo(tuPair.right.toString()); //
    }
}
