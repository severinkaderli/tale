package dev.tale.cli.datastructures;

import lombok.Value;

@Value
public class Vertex {
    public final int x;
    public final int y;

    public Vertex(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
