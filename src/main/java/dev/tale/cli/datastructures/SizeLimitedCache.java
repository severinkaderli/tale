package dev.tale.cli.datastructures;

import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.function.Function;

/**
 * Wraps a ConcurrentHashMap which evicts the oldest item when the
 * maxSize is exceeded.
 * @param <K> Key Type
 * @param <V> Value Type
 */
public class SizeLimitedCache<K, V> {
    private final int maxSize;
    private final Map<K, V> cache;
    private final Queue<K> keys;

    /**
     * {@inheritDoc}
     * Instantiates the cache with a default size of 64.
     */
    public SizeLimitedCache() {
        this(64);
    }

    /**
     * {@inheritDoc}
     * @param maxSize The maximum size
     */
    public SizeLimitedCache(int maxSize) {
        this.maxSize = maxSize;
        this.cache = new ConcurrentHashMap<>(maxSize);
        this.keys = new ConcurrentLinkedDeque<>();
    }

    public V put(K key, V value) {
        if(!cache.containsKey(key)) {
            this.keys.add(key);
            if(this.keys.size() > maxSize) {
                this.cache.remove(this.keys.poll());
            }
        }
        return this.cache.put(key, value);
    }

    public V computeIfAbsent(K key, Function<? super K, ? extends V> function) {
        return this.cache.computeIfAbsent(key, function);
    }

    public V get(K key) {
        return this.cache.get(key);
    }
}
