package dev.tale.cli.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * Handles the following use-cases:
 *  - hashing (using SHA-256)
 *  - convertion from/to base64
 *  - converting bytes to hex strings
 */
public class EncodingUtil {
    private static MessageDigest digest;
    private static final char[] HEX_DIGITS = "0123456789abcdef".toCharArray();
    private static final Base64.Encoder ENCODER_B64 = Base64.getEncoder();
    private static final Base64.Decoder DECODER_B64 = Base64.getDecoder();

    public static String hash(String input) {
        return toHexString(hash(input.getBytes()));
    }

    public static String toHexString(byte[] input) {
        char[] output = new char[input.length * 2];
        for(int i = 0; i < input.length; i++) {
            int value = input[i] & 0xFF;
            output[i * 2] = HEX_DIGITS[value >>> 4];
            output[i * 2 + 1] = HEX_DIGITS[value & 0x0F];
        }
        return new String(output);
    }

    public static byte[] hash(byte[] input) {
        if(digest == null) {
            try {
                digest = MessageDigest.getInstance("SHA-256");
            } catch (NoSuchAlgorithmException e) {
                throw new IllegalStateException("Couldn't hash input, because the system lacks support for SHA-256");
            }
        }
        return digest.digest(input);
    }

    public static String toBase64(String input) {
        if(input == null || input.isEmpty()) {
            return "";
        }
        return ENCODER_B64.encodeToString(input.getBytes());
    }

    public static String fromBase64(String input) {
        if(input.equals("")) {
            return "";
        }
        return new String(DECODER_B64.decode(input));
    }
}
