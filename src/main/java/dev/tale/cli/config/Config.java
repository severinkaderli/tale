package dev.tale.cli.config;

import com.electronwill.nightconfig.core.conversion.ObjectConverter;
import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import dev.tale.cli.command.ExitCode;
import dev.tale.cli.util.FileUtil;
import lombok.Data;

import java.nio.file.Path;
import java.time.format.DateTimeFormatter;

@Data
public class Config {
    /**
     * The name used for commits.
     */
    private String user;

    /**
     * The format used for displaying dates.
     */
    private String dateFormat;

    /**
     * The instance for the singleton.
     */
    private static Config INSTANCE;

    /**
     * Make default constructor private.
     */
    private Config() {
    }

    /**
     * Gets the singleton instance
     *
     * @return The instance of the singleton
     */
    public static Config getInstance() {
        if (INSTANCE == null) {
            init();
        }
        return INSTANCE;
    }

    /**
     * Get a value from the config by key. If the key doesn't exist the application exits
     * with exit code 1.
     *
     * @param key The key to get the value from.
     * @return The value of the key
     */
    public String get(String key) {
        this.checkKey(key);

        CommentedFileConfig config = loadConfig();
        String value = config.get(key);
        config.close();

        return value;
    }

    /**
     * Set a key in the configuration to a value.
     *
     * @param key   The key to set to value of.
     * @param value The value to set.
     */
    public void set(String key, String value) {
        this.checkKey(key);

        CommentedFileConfig config = loadConfig();
        config.set(key, value);
        config.save();
        Config.setInstance(config);
        config.close();
    }

    /**
     * Get a date time formatter based of the config date format.
     *
     * @return The date time formatter.
     */
    public DateTimeFormatter getDateFormatter() {
        return DateTimeFormatter.ofPattern(this.dateFormat);
    }

    /**
     * Initializes the config with default values and sets the instance.
     */
    private static void init() {
        CommentedFileConfig config = loadConfig();

        // Initialise the config if it's empty
        if (config.getFile().length() <= 1) {
            config.set("user", System.getProperty("user.name"));
            config.setComment("user", " The name used for commits");
            config.set("dateFormat", "yyyy-MM-dd HH:mm:ss");
            config.setComment("dateFormat", " The format used for displaying dates");
            config.save();
        }

        Config.setInstance(config);

        config.close();
    }

    /**
     * Loads the config from a file.
     *
     * @return Config object built from the config file.
     */
    private static CommentedFileConfig loadConfig() {
        Path configFile = FileUtil.findFileInDotTale("config.toml");
        CommentedFileConfig config = CommentedFileConfig.of(configFile);
        config.load();
        return config;
    }

    /**
     * Updates the instance with an object representation of the given config.
     *
     * @param config The config to be converted into an object.
     */
    private static void setInstance(CommentedFileConfig config) {
        ObjectConverter converter = new ObjectConverter();
        Config.INSTANCE = converter.toObject(config, Config::new);
    }

    /**
     * Check if the config contains the key.
     *
     * @param key The key to check.
     */
    private void checkKey(String key) {
        CommentedFileConfig config = loadConfig();
        if (config.get(key) == null) {
            System.err.printf("error: config does not contain a section '%s'", key);
            config.close();
            System.exit(ExitCode.ERROR);
        }
        config.close();
    }
}
