package dev.tale.cli.di;

import dev.tale.cli.exceptions.ApplicationException;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class ServiceFactory {
    private static final Map<Class<?>, ServiceProvider<?>> instances = new ConcurrentHashMap<>();

    public static <T> T getInstance(Class<? extends ServiceProvider<T>> clazz) {
        return (T) instances.get(clazz);
    }

    public static <T> T get(Class<?> clazz) {
        return get(clazz, new Stack<>());
    }

    private static <T> T get(Class<?> clazz, Stack<Class<?>> previous) {
        try {
            previous.push(clazz);
            if(!ServiceProvider.class.isAssignableFrom(clazz)) {
                throw new IllegalArgumentException(clazz.getSimpleName() + " is not a Service");
            }
            ServiceProvider<T> instance = (ServiceProvider<T>) clazz.newInstance();
            for(Class<?> dependency : instance.getDependencies()) {
                checkForCircularDependencies(dependency, previous);
                get(dependency, previous);
            }

            inject(instance);
            instance.init();
            instances.putIfAbsent(clazz, instance);

            previous.pop();
            return instance.getInstance();
        } catch (Exception e) {
            throw new ApplicationException("Couldn't instantiate " + clazz.getName(), e);
        }
    }

    private static void checkForCircularDependencies(Class<?> dependency, Stack<Class<?>> previous) {
        if(previous.contains(dependency)) {
            previous.push(dependency);
            throw new IllegalStateException("Circular Dependency! " +
                    previous.stream().map(Class::getSimpleName).collect(Collectors.joining(" => ")));
        }
    }

    public static void putMock(Class<? extends ServiceProvider<?>> clazz, ServiceProvider<?> mock) {
        instances.put(clazz, mock);
    }

    /**
     * For test use only!
     * Resets all instances.
     */
    @Deprecated()
    public static void reset() {
        instances.clear();
    }

    public static void inject(Object injectable) {
        try {
            List<Field> injectableFields = getInjectableFields(injectable.getClass());
            for (Field field : injectableFields) {
                field.setAccessible(true);
                field.set(injectable, instances.get(field.getType()));
            }
        } catch (Exception e) {
            throw new ApplicationException("Couldn't inject " + injectable.getClass().getSimpleName(), e);
        }
    }

    private static List<Field> getInjectableFields(Class<?> clazz) {
        return Arrays.stream(clazz.getDeclaredFields()) //
                .filter(field -> Arrays.stream(field.getDeclaredAnnotationsByType(Inject.class)).findAny().isPresent()) //
                .collect(Collectors.toList());
    }
}
