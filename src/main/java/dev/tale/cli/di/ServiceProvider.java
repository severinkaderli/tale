package dev.tale.cli.di;

public interface ServiceProvider<T> {
    void init();
    T getInstance();
    Class<?>[] getDependencies();
}
