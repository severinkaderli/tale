package dev.tale.cli.repository;

import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.repository.object.TreeObject;
import dev.tale.cli.util.EncodingUtil;
import dev.tale.cli.util.FileUtil;

import java.nio.file.Path;

/**
 * Represents the staged TreeObject
 */
public class StagingArea implements ServiceProvider<StagingArea> {
    @Inject
    private TreeAccessor treeAccessor;
    public final Path STAGED = FileUtil.findFileInDotTale("staged");

    @Override
    public void init() {
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[]{TreeAccessor.class};
    }

    public StagingArea getInstance() {
        return this;
    }

    public String getStagedId() {
        return Reference.resolve(STAGED);
    }

    public TreeObject getOrCreateStagingTreeObject() {
        String id = getStagedId();

        if(id == null) {
            TreeObject treeObject = new TreeObject(
                    TreeAccessor.getHeadId() == null ? Reference.NULL_REFERENCE : TreeAccessor.getHeadId(),
                    0,
                    null
            );

            return updateStagingTreeObject(treeObject);
        } else {
            return treeAccessor.getTreeObject(id);
        }
    }

    public void reset() {
        FileUtil.writeContentToPath(STAGED, Reference.NULL_REFERENCE);
    }

    public TreeObject updateStagingTreeObject(TreeObject object) {
        // calculate hash
        String c = object.toDiskRepresentation();
        //byte[] diskBytes = object.toDiskBytes();
        byte[] diskBytes = c.getBytes();
        String hash = EncodingUtil.toHexString(EncodingUtil.hash(diskBytes));

        // get object path
        Path path = Reference.toPath(hash);
        object.setFile(path.toFile());

        // write object
        FileUtil.writeContentToPath(path, diskBytes);

        // write pointer
        FileUtil.writeContentToPath(STAGED, hash);

        return object;
    }

}
