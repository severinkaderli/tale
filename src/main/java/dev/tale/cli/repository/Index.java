package dev.tale.cli.repository;

import dev.tale.cli.datastructures.Pair;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.exceptions.ApplicationException;
import dev.tale.cli.util.EncodingUtil;
import dev.tale.cli.util.FileUtil;
import dev.tale.cli.util.PathUtil;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Index implements ServiceProvider<Index> {
    private static final Pattern PATTERN = Pattern.compile("(.*)\\s([0-9a-f]{64})\\s([0-9a-f]{64})");
    private Path indexFile;
    private Map<Path, Pair<String, String>> cache;

    @Override
    public void init() {
        this.cache = new HashMap<>();
        this.indexFile = FileUtil.findFileInDotTale("index");

        try(Stream<String> stream = Files.lines(Objects.requireNonNull(this.indexFile))) {
            stream.map(PATTERN::matcher) //
                    .filter(Matcher::matches) //
                    .forEach(matcher -> {
                        cache.put(Paths.get(matcher.group(1)), new Pair<>(matcher.group(2), matcher.group(3)));
                    });
        } catch (IOException | NullPointerException e) {
            throw new ApplicationException("Couldn't read index", e.getCause());
        }
    }
    @Override
    public Index getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class<?>[0];
    }

    public boolean isDeleted(Path path) {
        return !this.cache.containsKey(path) || this.cache.get(path).getRight().equals(Reference.NULL_REFERENCE);
    }

    public String getState(Path path) {
        if(!cache.containsKey(path)) {
            throw new ApplicationException("\"" + path + "\" is not in the index.");
        }
        return cache.get(path).getLeft();
    }

    public void putState(Path path, String ID) {
        String fileHash;
        try {
            fileHash = EncodingUtil.toHexString(EncodingUtil.hash(FileUtil.loadFileFromPathAsBytes(path)));
        } catch (ApplicationException e) {
            // the file has been deleted, the hash is now NIL
            fileHash = Reference.NULL_REFERENCE;
        }
        cache.put(PathUtil.relativize(path), new Pair<>(ID, fileHash));
        write();
    }

    public boolean isSameAsIndex(Path path) {
        Path relative = PathUtil.relativize(path);
        if(cache.containsKey(relative) && relative.toFile().isFile()) {
            String fileHash;
            try {
                fileHash = EncodingUtil.toHexString(EncodingUtil.hash(FileUtil.loadFileFromPathAsBytes(relative)));
            }catch (ApplicationException e) {
                fileHash = Reference.NULL_REFERENCE;
            }
            String cacheHash = cache.get(relative).getRight();

            if(cacheHash.equals(Reference.NULL_REFERENCE) && !fileHash.equals(Reference.NULL_REFERENCE)) {
                return false;
            }
            return cacheHash.equals(Reference.NULL_REFERENCE) || cacheHash.equals(fileHash);
        }
        return false;
    }

    public void removeFromIndex(Path path) {
        cache.remove(path);
        write();
    }

    private void write() {
        byte[] content = cache.entrySet().stream() //
            .map(entry -> entry.getKey() + " " + entry.getValue().getLeft() + " " + entry.getValue().getRight()) //
            .collect(Collectors.joining("\n")).getBytes();
        FileUtil.writeContentToPath(this.indexFile, content);
    }

    public Set<Path> files() {
        return new HashSet<>(cache.keySet());
    }
}
