package dev.tale.cli.repository.object;

import dev.tale.cli.patch.Patch;
import dev.tale.cli.serialization.Serialization;
import dev.tale.cli.util.EncodingUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Builder
@AllArgsConstructor
public class Content {
    private static final String NEWLINE = System.lineSeparator();
    private static final String SEPARATOR = ",";

    private final String[] blob;
    private String author;
    private String message;
    private List<Patch> patches;

    public Content(String content) {
        if(content.isEmpty()) {
            this.blob = new String[0];
        } else {
            this.blob = EncodingUtil.fromBase64(content).split(NEWLINE);
        }
    }

    public String getAuthor() {
        if(author == null && blob.length >= 1) {
            author = blob[0];
        }
        return author;
    }

    public String getMessage() {
        if(message == null && blob.length >= 2) {
            message = EncodingUtil.fromBase64(blob[1]);
        }
        return message;
    }

    public List<Patch> getPatches() {
        if(patches == null) {
            if(blob.length == 3) {
                String[] encoded = blob[2].split(SEPARATOR);
                if(encoded.length >= 1) {
                    patches = Arrays.stream(encoded) //
                            .map(Serialization::difference) //
                            .collect(Collectors.toList());
                    return patches;
                }
            }
            patches = new ArrayList<>();
        }
        return patches;
    }

    public String toBlob() {
        StringBuilder sb = new StringBuilder();
        sb.append(getAuthor()).append(NEWLINE);
        sb.append(EncodingUtil.toBase64(getMessage())).append(NEWLINE);

        String diffsAsString = this.getPatches().stream() //
                .map(Serialization::difference) //
                .collect(Collectors.joining(SEPARATOR));

        sb.append(diffsAsString).append(NEWLINE);
        return EncodingUtil.toBase64(sb.toString());
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void addPatch(Patch patch) {
        this.getPatches().add(patch);
    }

    public void setPatches(List<Patch> patches) {
        this.patches = patches;
    }
}

