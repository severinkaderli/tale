package dev.tale.cli.repository;

import dev.tale.cli.exceptions.ApplicationException;
import dev.tale.cli.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Stack;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Reference {
    private static final Pattern OBJECT_REF = Pattern.compile("^[a-f0-9]+$");
    public static final String NULL_REFERENCE = "0000000000000000000000000000000000000000000000000000000000000000";

    public static Stack<String> resolveHierarchy(Path pointer, Stack<String> hierarchy) {
        try {
            String content = Files.readAllLines(pointer).get(0);
            if (content.equals(Reference.NULL_REFERENCE)) {
                return hierarchy;
            }

            hierarchy.push(content);

            return resolveHierarchy(toPath(content), hierarchy);
        } catch (IOException e) {
            throw new ApplicationException("Failed to resolve reference.", e);
        }
    }

    /**
     * Recursively resolves references in Tree Files
     */
    public static String resolve(Path pointer) {
        try {
            String content = Files.readAllLines(pointer).get(0);
            if (content.equals(Reference.NULL_REFERENCE)) {
                return null; // this is a null-reference
            }

            if (OBJECT_REF.matcher(content).matches()) {
                return content;
            }

            return resolve(FileUtil.findFileInDotTale(content));
        } catch (IOException e) {
            throw new ApplicationException("Failed to resolve reference.", e);
        }
    }

    public static Path toPath(String reference) {
        if (reference == null) {
            return null;
        }
        if (OBJECT_REF.matcher(reference).matches()) {
            if (reference.length() < NULL_REFERENCE.length()) {
                return resolvePartial(reference);
            }
            return FileUtil.findFileInDotTale("objects", reference.substring(0, 2), reference.substring(2));
        } else {
            return FileUtil.findFileInDotTale(reference);
        }
    }

    public static Path resolvePartial(String reference) {
        if (reference.length() < 4) {
            throw new ApplicationException("Cannot resolve object-refs shorter than 4 characters. Received: " + reference);
        } else {
            String firstByte = reference.substring(0, 2);
            String remainingBytes = reference.substring(2);
            Path bucket = FileUtil.findFileInDotTale("objects", firstByte);
            List<Path> candidates = FileUtil.findFilesRecursively(bucket, file -> file.getName().startsWith(remainingBytes));
            if (candidates.size() == 1) {
                return candidates.get(0);
            } else if (candidates.size() == 0) {
                throw new ApplicationException(reference + " is not a known object-ref.");
            } else {
                throw new ApplicationException(reference + " is an ambiguous object-ref. Found the following candidates: " +
                        candidates.stream().map(Path::toFile).map(File::getName).collect(Collectors.joining(",")));
            }
        }
    }

    static List<String> reverse(Stack<String> input) {
        List<String> result = new Stack<>();
        while (!input.empty()) {
            result.add(input.pop());
        }

        return result;
    }
}
