package dev.tale.cli.repository;

import dev.tale.cli.datastructures.Pair;
import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.repository.object.TreeObject;
import dev.tale.cli.util.FileUtil;
import dev.tale.cli.util.PathUtil;

import java.nio.file.Path;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static dev.tale.cli.util.FileUtil.findFileInDotTale;
import static dev.tale.cli.util.FileUtil.loadFileFromPath;

public class Repository implements ServiceProvider<Repository> {
    @Inject
    private Index index;

    @Inject
    private StagingArea stagingArea;

    @Override
    public void init() {
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class<?>[]{Index.class, StagingArea.class};
    }

    @Override
    public Repository getInstance() {
        return this;
    }

    public String getCurrentBranch() {
        return FileUtil.unsanitize(loadFileFromPath(findFileInDotTale("HEAD")).get(0)).trim();
    }

    public Collection<Pair<Path, Path>> listStagedFiles() {
        TreeObject staged = stagingArea.getOrCreateStagingTreeObject();

        return staged.getContentAsObject().getPatches().stream() //
                .map(patch -> new Pair<>( //
                        PathUtil.relativize(patch.from), //
                        PathUtil.relativize(patch.to) //
                )) //
                .distinct() //
                .collect(Collectors.toList());
    }

    public Collection<Pair<Path, Path>> listUntrackedFiles() {
        Set<Path> allFiles = new HashSet<>(FileUtil.findFilesRecursively(PathUtil.getInstance().getRoot()));
        Set<Path> indexFiles = index.files();
        allFiles.removeIf(file -> indexFiles.contains(PathUtil.relativize(file)));
        return allFiles.stream() //
                .map(PathUtil::relativize) //
                .sorted(Comparator.comparing(Path::toString)) //
                .map(path -> new Pair<>(PathUtil.DEV_NULL, path)) //
                .collect(Collectors.toList());
    }

    public Collection<Pair<Path, Path>> listChangedFiles() {
        String stagedId = stagingArea.getStagedId();
        Collection<Pair<Path, Path>> stagedFiles = listStagedFiles();

        return this.index.files().stream() //
            .filter(path -> !this.index.isSameAsIndex(path)) //
            .filter(path -> !this.index.isDeleted(path)) //
            .map(path -> {
                if(stagedId != null && stagedId.equals(this.index.getState(path))) {
                    // FIXME this is extraordinarily ugly
                    // check if the file is new
                    if(stagedFiles.stream().anyMatch(pair -> pair.getLeft().equals(PathUtil.DEV_NULL) && pair.getRight().equals(path))) {
                        return new Pair<>(PathUtil.DEV_NULL, path);
                    }
                }
                    return new Pair<>(path, path);
            }).collect(Collectors.toList());
    }
}
