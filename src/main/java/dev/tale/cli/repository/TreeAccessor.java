package dev.tale.cli.repository;

import dev.tale.cli.datastructures.SizeLimitedCache;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.repository.object.Content;
import dev.tale.cli.repository.object.TreeObject;
import dev.tale.cli.util.FileUtil;

import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Cached Access to TreeObjects and their content.
 */
public class TreeAccessor implements ServiceProvider<TreeAccessor> {
    private SizeLimitedCache<String, TreeObject> treeObjectCache;
    private SizeLimitedCache<String, Content> contentCache;
    private static final int CACHE_SIZE = 64;
    private static Path HEAD;
    private static int minIdLength = -1;

    @Override
    public void init() {
        this.treeObjectCache = new SizeLimitedCache<>(CACHE_SIZE);
        this.contentCache = new SizeLimitedCache<>(CACHE_SIZE);
    }

    @Override
    public TreeAccessor getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[0];
    }

    public TreeObject getTreeObject(String id) {
        return this.treeObjectCache.computeIfAbsent(id, k -> {
            Path path = Reference.toPath(k);
            return TreeObject.read(path.toFile());
        });
    }

    public Content getContent(String id) {
        return this.contentCache.computeIfAbsent(id, k -> getTreeObject(k).getContentAsObject());
    }

    public static String getHeadId() {
        if(HEAD == null) {
            HEAD = FileUtil.findFileInDotTale("HEAD");
        }
        return Reference.resolve(HEAD);
    }

    public static int getMinIdLength() {
        if(minIdLength == -1) {
            minIdLength = Math.max(10, findShortestUniqueIdLength());
        }
        return minIdLength;
    }

    private static int findShortestUniqueIdLength() {
        List<String> objectIds = FileUtil.findFilesRecursively(FileUtil.findFileInDotTale("objects")).stream()
                .map(path -> {
                    int count = path.getNameCount();
                    return path.getName(count - 2).toString() + path.getName(count - 1).toString();
                })
                .collect(Collectors.toList());

        for (int i = 1; i <= Reference.NULL_REFERENCE.length(); i++) {
            final int lengthToCheck = i; // required because only effectively final variables can be used in lambdas
            Set<String> ids = objectIds.stream() //
                    .map(id -> id.substring(0, lengthToCheck)) //
                    .collect(Collectors.toSet());
            if (ids.size() == objectIds.size()) { // no duplicates using this length
                return i;
            }
        }
        return Reference.NULL_REFERENCE.length();
    }
}
