package dev.tale.cli.serialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dev.tale.cli.patch.Hunk;
import dev.tale.cli.patch.HunkHeader;
import dev.tale.cli.patch.Patch;
import dev.tale.cli.util.EncodingUtil;

import java.nio.file.Path;

public class Serialization {
    private static Serialization instance;

    private final Gson gson;

    public static Serialization getInstance() {
        if (instance == null) {
            instance = new Serialization();
        }
        return instance;
    }

    private Serialization() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(HunkHeader.class, new HunkHeaderSerializer());
        builder.registerTypeAdapter(Hunk.class, new HunkSerializer());
        builder.registerTypeAdapter(Path.class, new PathSerializer());
        builder.registerTypeAdapter(Patch.class, new PatchSerializer());

        this.gson = builder.create();
    }

    public static Gson gson() {
        return getInstance().gson;
    }

    public static String difference(Patch difference) {
        String json = getInstance().gson.toJson(difference, Patch.class);
        return EncodingUtil.toBase64(json);
    }

    public static Patch difference(String input) {
        String json = EncodingUtil.fromBase64(input);
        return getInstance().gson.fromJson(json, Patch.class);
    }
}
