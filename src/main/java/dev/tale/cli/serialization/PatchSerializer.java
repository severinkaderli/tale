package dev.tale.cli.serialization;

import com.google.gson.*;
import dev.tale.cli.patch.Hunk;
import dev.tale.cli.patch.Patch;

import java.lang.reflect.Type;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

public class PatchSerializer implements JsonSerializer<Patch>, JsonDeserializer<Patch> {
    private static final String FROM = "from";
    private static final String TO = "to";
    private static final String HUNKS = "hunks";

    @Override
    public Patch deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject json = jsonElement.getAsJsonObject();

        List<Hunk> hunks = new LinkedList<>();
        json.getAsJsonArray(HUNKS).forEach(jsonHunk -> {
            hunks.add(context.deserialize(jsonHunk, Hunk.class));
        });

        return Patch.builder() //
            .from(context.deserialize(json.get(FROM), Path.class)) //
            .to(context.deserialize(json.get(TO), Path.class)) //
            .hunks(hunks) //
            .build();
    }

    @Override
    public JsonElement serialize(Patch patch, Type type, JsonSerializationContext context) {
        JsonObject json = new JsonObject();
        json.add(FROM, context.serialize(patch.getFrom(), Path.class));
        json.add(TO, context.serialize(patch.getTo(), Path.class));
        json.add(HUNKS, context.serialize(patch.getHunks()));
        return json;
    }
}
