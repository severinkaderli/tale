package dev.tale.cli.serialization;

import com.google.gson.*;
import dev.tale.cli.patch.HunkHeader;

import java.lang.reflect.Type;

public class HunkHeaderSerializer implements JsonSerializer<HunkHeader>, JsonDeserializer<HunkHeader> {

    @Override
    public HunkHeader deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        return HunkHeader.fromString(jsonElement.getAsString());
    }

    @Override
    public JsonElement serialize(HunkHeader hunkHeader, Type type, JsonSerializationContext context) {
        return context.serialize(hunkHeader.toString());
    }
}
