package dev.tale.cli.serialization;

import com.google.gson.*;
import dev.tale.cli.util.PathUtil;

import java.lang.reflect.Type;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PathSerializer implements JsonSerializer<Path>, JsonDeserializer<Path> {
    @Override
    public Path deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        return Paths.get(jsonElement.getAsString());
    }

    @Override
    public JsonElement serialize(Path path, Type type, JsonSerializationContext context) {
        return context.serialize(PathUtil.relativize(path).toString());
    }
}
