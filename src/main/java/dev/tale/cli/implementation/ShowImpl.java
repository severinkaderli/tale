package dev.tale.cli.implementation;

import dev.tale.cli.command.ExitCode;
import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.repository.Reference;
import dev.tale.cli.repository.TreeAccessor;
import dev.tale.cli.repository.object.TreeObject;

import java.nio.file.Path;

public class ShowImpl implements ServiceProvider<ShowImpl> {

    @Inject
    private TreeAccessor treeAccessor;

    @Override
    public void init() {

    }

    @Override
    public ShowImpl getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[]{TreeAccessor.class};
    }

    public Integer showCommit(String ref, boolean verbose) {
        String id;
        if (ref.equals("HEAD")) {
            id = TreeAccessor.getHeadId();
        } else {
            Path path = Reference.resolvePartial(ref);
            int len = path.getNameCount();
            id = path.getName(len - 2).toString() + path.getName(len - 1).toString();
        }

        TreeObject object = treeAccessor.getTreeObject(id);
        System.out.println(object.getLogEntry(false, verbose));
        System.out.println(object.getPatches());

        return ExitCode.SUCCESS;
    }
}
