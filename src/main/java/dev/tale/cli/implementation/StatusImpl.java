package dev.tale.cli.implementation;

import dev.tale.cli.command.ExitCode;
import dev.tale.cli.datastructures.Pair;
import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.repository.Repository;
import dev.tale.cli.util.PathUtil;

import java.nio.file.Path;
import java.util.Collection;

public class StatusImpl implements ServiceProvider<StatusImpl> {

    @Inject
    private Repository repository;

    @Override
    public void init() {

    }

    @Override
    public StatusImpl getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[] {Repository.class};
    }

    public Integer printStatus() {
        printStagedFiles();

        printChangedFiles();

        printUntrackedFiles();

        return ExitCode.SUCCESS;
    }

    private void printStagedFiles() {
        Collection<Pair<Path, Path>> pairs = repository.listStagedFiles();
        if(!pairs.isEmpty()) {
            System.out.println("Changes to be commited:");
            pairs.stream() //
                    .sorted() //
                    .forEach(pair -> {
                        if (PathUtil.DEV_NULL.equals(pair.getLeft())) {
                            // new
                            System.out.println("\tnew file:\t" + pair.getRight());
                        } else if(isDeletion(pair)) {
                            // deleted
                            System.out.println("\tdeleted:\t" + pair.getLeft());
                        } else if(pair.getLeft().equals(pair.getRight())){
                            // changed
                            System.out.println("\tmodified:\t" + pair.getRight());
                        } else {
                            // renamed
                            System.out.println("\trenamed:\t" + pair.getLeft() + " -> " + pair.getRight());
                        }
                    });
        }
    }

    private void printChangedFiles() {
        Collection<Pair<Path, Path>> changedFiles = repository.listChangedFiles();
        if(!changedFiles.isEmpty()) {
            System.out.println("Changes not staged for commit:");
            changedFiles.forEach(pair -> {
                if(isDeletion(pair)) {
                    System.out.println("\tdeleted:\t" + pair.getLeft());
                } else {
                    System.out.println("\tmodified:\t" + pair.getRight());
                }
            });
        }
    }

    private void printUntrackedFiles() {
        Collection<Pair<Path, Path>> untrackedFiles = repository.listUntrackedFiles();
        if(!untrackedFiles.isEmpty()) {
            System.out.println("Untracked Files:");
            untrackedFiles.forEach(pair -> System.out.println("\t" + pair.getRight()));
        }
    }

    private boolean isDeletion(Pair<Path, Path> pair) {
        return PathUtil.DEV_NULL.equals(pair.getRight());
    }
}
