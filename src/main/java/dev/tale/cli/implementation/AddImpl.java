package dev.tale.cli.implementation;

import dev.tale.cli.command.ExitCode;
import dev.tale.cli.config.Config;
import dev.tale.cli.datastructures.Pair;
import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.patch.Patch;
import dev.tale.cli.patch.PatchGenerator;
import dev.tale.cli.repository.*;
import dev.tale.cli.repository.object.Content;
import dev.tale.cli.repository.object.TreeObject;
import dev.tale.cli.util.FileUtil;

import java.nio.file.Path;
import java.util.*;

import static dev.tale.cli.util.PathUtil.DEV_NULL;

public class AddImpl implements ServiceProvider<AddImpl> {

    @Inject
    private History history;

    @Inject
    private Repository repository;

    @Inject
    private StagingArea stagingArea;

    @Inject
    private Index index;

    @Override
    public void init() {

    }

    @Override
    public AddImpl getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[]{
                History.class,
                Repository.class,
                StagingArea.class,
                Index.class
        };
    }

    public Integer noOptionsAdd(Path... pathsToAdd) {
        Collection<Pair<Path, Path>> untracked = repository.listUntrackedFiles();
        Collection<Pair<Path, Path>> changed = repository.listChangedFiles();
        Collection<Pair<Path, Path>> staged = repository.listStagedFiles();
        Set<Path> toAdd = new HashSet<>(Arrays.asList(pathsToAdd));

        Set<Pair<Path, Path>> untrackedToAdd = new HashSet<>(untracked);
        untrackedToAdd.removeIf(pair -> !toAdd.contains(pair.getRight()));

        Set<Pair<Path, Path>> changedToAdd = new HashSet<>(changed);
        changedToAdd.removeIf(pair -> !toAdd.contains(pair.getLeft()) && !toAdd.contains(pair.getRight()));

        TreeObject stagedObject = stagingArea.getOrCreateStagingTreeObject();
        Content stagedContent;
        if(stagedObject.getContent().isEmpty()) {
            stagedContent = new Content(new String[0], Config.getInstance().getUser(), "", new ArrayList<>());
        } else {
            stagedContent = new Content(stagedObject.getContent());
        }

        Set<Path> newlyStaged = new HashSet<>();

        for(Pair<Path, Path> file : untrackedToAdd) {
            Patch patch = PatchGenerator.generate(DEV_NULL, file.getRight());
            stagedContent.addPatch(patch);
            newlyStaged.add(file.getRight());
        }

        String headId = TreeAccessor.getHeadId();

        for(Pair<Path, Path> file : changedToAdd) {
            List<String> historicState;
            if(headId == null) {
                // there is no history yet
                historicState = new LinkedList<>();
            } else {
                historicState = history.getHistoricState(file.getRight(), headId);
            }
            Patch patch = PatchGenerator.generate(
                    file.getLeft(),
                    file.getRight(),
                    historicState,
                    FileUtil.loadFileFromPath(file.getRight()));


            stagedContent.getPatches().removeIf(oldPatch -> oldPatch.isForFile(file));
            stagedContent.addPatch(patch);
            newlyStaged.add(file.getRight());
        }

        stagedObject.setContent(stagedContent.toBlob());
        stagedObject.updateTimestamp();
        stagingArea.updateStagingTreeObject(stagedObject);

        for(Path file : newlyStaged) {
            index.putState(file, stagingArea.getStagedId());
        }

        return ExitCode.SUCCESS;
    }
}
