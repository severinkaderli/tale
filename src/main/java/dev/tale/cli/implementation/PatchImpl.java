package dev.tale.cli.implementation;

import dev.tale.cli.command.ExitCode;
import dev.tale.cli.patch.Patch;
import dev.tale.cli.patch.Patcher;
import dev.tale.cli.util.FileUtil;
import dev.tale.cli.util.PathUtil;

import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

public class PatchImpl {

    public Integer patchIndex(final Path patchPath) {
        // TODO implement
        throw new UnsupportedOperationException("Patching the index isn't supported yet.");
    }

    public Integer patchFiles(final Path patchPath, final Path[] paths, boolean verbose) {
        try{
            Map<Path, AtomicInteger> statistics = patch(patchPath, paths);

            if(verbose) {
                System.out.println("Successfully applied the following patches:");
                System.out.println("Patches  Path");
                statistics.entrySet().stream() //
                        .map(e -> String.format("%7d  %s", e.getValue().get(), e.getKey())) //
                        .forEach(System.out::println);
            }
            return ExitCode.SUCCESS;
        } catch (Exception e) {
            return ExitCode.ERROR;
        }
    }

    public Map<Path, AtomicInteger> patch(final Path patchPath, final Path... paths) {
        List<String> patchContent = FileUtil.loadFileFromPath(PathUtil.relativize(patchPath));

        Map<Path, List<Patch>> patches = generatePatches(patchContent);
        Map<Path, AtomicInteger> statistics = new HashMap<>(patches.size());

        Arrays.stream(paths) //
                .map(PathUtil::relativize) //
                .collect(Collectors.toMap(Function.identity(), FileUtil::loadFileFromPath)) //
                .entrySet().stream() //
                .map(entry -> {
                    List<String> content = entry.getValue();
                    Path path = entry.getKey();
                    if(patches.containsKey(path)) {
                        patches.get(path).stream() //
                                .map(Patch::getHunks) //
                                .map(Patcher::new) //
                                .peek(patcher -> patcher.applyTo(content))
                                .forEach(unused -> statistics.computeIfAbsent(path, u -> new AtomicInteger(0)).incrementAndGet());
                    }
                    return new AbstractMap.SimpleEntry<>(path, content);
                }).forEach(entry -> FileUtil.writeContentToPath(entry.getKey(), entry.getValue()));
        return statistics;
    }

    private Map<Path, List<Patch>> generatePatches(List<String> patchContent) {
        Map<Path, List<Patch>> patches = new HashMap<>();

        int start = 0;

        for(int i = 2; i < patchContent.size(); i++) {
            if(isHeader(patchContent, i)) {
                Patch patch = Patch.fromCLIRepresentation(patchContent.subList(start, i - 2));
                patches.computeIfAbsent(patch.getFrom(), unused -> new LinkedList<>()).add(patch);

                start = i - 1;
            }
        }
        Patch patch = Patch.fromCLIRepresentation(patchContent.subList(start, patchContent.size()));
        patches.computeIfAbsent(patch.getFrom(), unused -> new LinkedList<>()).add(patch);

        return patches;
    }

    private boolean isHeader(List<String> content, int index) {
        if(index < 2 || content.size() < 3) {
            throw new IllegalArgumentException("Cannot read this file as a patch");
        }

        return content.get(index - 2).matches("\\R") //
                && content.get(index - 1).startsWith(Patch.FROM_HEADER) //
                && content.get(index).startsWith(Patch.TO_HEADER);
    }
}
