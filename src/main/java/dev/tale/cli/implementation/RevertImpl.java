package dev.tale.cli.implementation;

import dev.tale.cli.command.ExitCode;
import dev.tale.cli.config.Config;
import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.exceptions.ApplicationException;
import dev.tale.cli.patch.Patch;
import dev.tale.cli.patch.Patcher;
import dev.tale.cli.repository.Index;
import dev.tale.cli.repository.Reference;
import dev.tale.cli.repository.Repository;
import dev.tale.cli.repository.TreeAccessor;
import dev.tale.cli.repository.object.Content;
import dev.tale.cli.repository.object.TreeObject;
import dev.tale.cli.util.EncodingUtil;
import dev.tale.cli.util.FileUtil;
import dev.tale.cli.util.PathUtil;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class RevertImpl implements ServiceProvider<RevertImpl> {

    @Inject
    private TreeAccessor treeAccessor;

    @Inject
    private Repository repo;

    @Inject
    private Index index;

    @Override
    public void init() {

    }

    @Override
    public RevertImpl getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[]{
                TreeAccessor.class,
                Repository.class,
                Index.class
        };
    }

    public Integer revert(String reference) {
        List<Patch> patches = new ArrayList<>();
        StringBuilder message = new StringBuilder();

        for(String ref : Collections.singleton(reference)) {
            patches.addAll(revert(resolve(ref), message));
        }

        Content.ContentBuilder builder = Content.builder();
        builder.author(Config.getInstance().getUser());
        builder.patches(patches);
        builder.message(message.toString());

        Content content = builder.build();

        // apply to the files
        content.getPatches().forEach(patch -> {
            if(PathUtil.isNullEquivalent(patch.getTo())) {
                index.removeFromIndex(patch.getFrom());
                if(!patch.from.toFile().delete()){
                    throw new ApplicationException("Couldn't delete " + patch.from.toString());
                }
            } else if (PathUtil.isNullEquivalent(patch.getFrom())) {
                List<String> newFile = new LinkedList<>();
                Patcher patcher = new Patcher(patch.getHunks());
                newFile = patcher.applyTo(newFile);
                FileUtil.writeContentToPath(patch.getTo(), newFile);
            } else if(patch.getFrom().equals(patch.getTo())) {
                List<String> file = FileUtil.loadFileFromPath(patch.getFrom());
                Patcher patcher = new Patcher(patch.getHunks());
                file = patcher.applyTo(file);
                FileUtil.writeContentToPath(patch.getTo(), file);
            } else {
                throw new ApplicationException("Renaming is not yet supported in reverts.");
            }
        });

        TreeObject treeObject = TreeObject.builder() //
                .parentID(TreeAccessor.getHeadId()) //
                .timestamp(System.currentTimeMillis()) //
                .content(content.toBlob()) //
                .build();//

        byte[] treeObjectBytes = treeObject.toDiskBytes();

        String hash = EncodingUtil.toHexString(EncodingUtil.hash(treeObjectBytes));

        // update index
        content.getPatches() //
                .forEach(patch -> {
                    if(PathUtil.DEV_NULL.equals(patch.getTo())) {
                        index.removeFromIndex(patch.getTo());
                    } else {
                        index.putState(patch.getTo(), hash);
                    }
                });


        // find the current branch
        String branch = repo.getCurrentBranch();

        // update the reference on the branch
        FileUtil.writeContentToPath(FileUtil.findFileInDotTale(branch), hash);

        // write the new commit object
        FileUtil.writeContentToPath(FileUtil.findFileInDotTale("objects",
                hash.substring(0, 2), hash.substring(2)), treeObjectBytes);

        return ExitCode.SUCCESS;
    }

    private String resolve(String ref) {
        if (ref.equals("HEAD")) {
            return TreeAccessor.getHeadId();
        } else {
            Path path = Reference.resolvePartial(ref);
            int len = path.getNameCount();
            return path.getName(len - 2).toString() + path.getName(len - 1).toString();
        }
    }

    private List<Patch> revert(String id, StringBuilder message) {
        Content content = treeAccessor.getContent(id);
        message.append("Revert \"").append(content.getMessage()).append("\"\n");
        return content.getPatches().stream().map(Patch::reverse).collect(Collectors.toList());
    }
}
