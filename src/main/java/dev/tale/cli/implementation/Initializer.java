package dev.tale.cli.implementation;

import dev.tale.cli.command.ExitCode;
import dev.tale.cli.config.Config;
import dev.tale.cli.repository.Reference;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

public class Initializer {
    public Integer initialize() {
        return this.initialize(Optional.empty());
    }

    public Integer initialize(Optional<String> workingDirectory) {
        File dotTale = Paths.get(workingDirectory.orElse(""), ".tale").toFile();
        if(isTaleRepository(dotTale)) {
            System.out.println("This is already a tale repository - doing nothing");
            return ExitCode.SUCCESS;
        } else if(dotTale.exists()){
            System.err.println("There is already a file called .tale - couldn't create a repository");
            return ExitCode.ERROR;
        }
        try {
            return initTaleRepository(dotTale);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            if(!dotTale.delete()) {
                System.err.println("Couldn't clean up after failing to init repository. Manual cleanup required.");
            }
            return ExitCode.ERROR;
        }
    }

    private int initTaleRepository(File dotTale) throws Exception {
        if(!dotTale.mkdir()) {
            return ExitCode.ERROR;
        }

        if(!dotTale.setWritable(true, false)) {
            System.err.println("Insufficient permission to create .tale with write permissions");
            return ExitCode.ERROR;
        }

        final String dotTalePath = dotTale.getAbsolutePath();

        try{
            createTaleDirectory(dotTalePath, "objects");
            createTaleDirectory(dotTalePath, "branches");
            createMasterBranch(dotTalePath);
            createHEAD(dotTalePath);
            createStaged(dotTalePath);
            createTaleFile(dotTalePath, "", "index");

            // Initialize config
            createTaleFile(dotTalePath, "", "config.toml");
            Config.getInstance();
        }catch (IllegalStateException e) {
            System.out.println(e.getMessage());
            return ExitCode.ERROR;
        }

        System.out.println("Successfully initialized empty tale repository in " + dotTalePath);

        return ExitCode.SUCCESS;
    }

    private void createMasterBranch(String dotTalePath) throws IOException, IllegalStateException {
        createTaleFile(dotTalePath, Reference.NULL_REFERENCE, "branches", "master");
    }

    private void createHEAD(String dotTalePath) throws IOException, IllegalStateException{
        createTaleFile(dotTalePath, "branches/master", "HEAD");
    }

    private void createStaged(String dotTalePath) throws IOException, IllegalStateException{
        createTaleFile(dotTalePath, Reference.NULL_REFERENCE, "staged");
    }

    private void createTaleDirectory(String dotTalePath, String... path) throws IllegalStateException{
        if(!Paths.get(dotTalePath, path).toFile().mkdir()) {
            throw new IllegalStateException("Couldn't create .tale directory " + Arrays.asList(path));
        }
    }

    private void createTaleFile(String dotTalePath, String content, String... path) throws IOException, IllegalStateException{
        createTaleFile(dotTalePath, Collections.singletonList(content), path);
    }

    private void createTaleFile(String dotTalePath, Collection<String> content, String... path) throws IOException, IllegalStateException{
        File file = Paths.get(dotTalePath, path).toFile();
        if(file.createNewFile() && Objects.nonNull(content)) {
            Files.write(file.toPath(), content, StandardCharsets.UTF_8, StandardOpenOption.WRITE);
        } else {
            throw new IllegalStateException("Couldn't create .tale file " + Arrays.asList(path) + " with content=" + content);
        }
    }

    private boolean isTaleRepository(File dotTale) {
        return dotTale.exists() //
                && dotTale.isDirectory() //
                && Paths.get(dotTale.getAbsolutePath(), "HEAD").toFile().exists();
    }
}
