package dev.tale.cli.implementation;

import dev.tale.cli.command.ExitCode;
import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.repository.Index;
import dev.tale.cli.repository.Repository;
import dev.tale.cli.repository.StagingArea;
import dev.tale.cli.repository.object.Content;
import dev.tale.cli.repository.object.TreeObject;
import dev.tale.cli.util.EncodingUtil;
import dev.tale.cli.util.FileUtil;
import dev.tale.cli.util.PathUtil;

public class CommitImpl implements ServiceProvider<CommitImpl> {

    @Inject
    private StagingArea stagingArea;

    @Inject
    private Index index;

    @Inject
    private Repository repository;

    @Override
    public void init() {

    }

    @Override
    public CommitImpl getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[]{
                StagingArea.class,
                Index.class,
                Repository.class
        };
    }

    public Integer commit(String message) {
        TreeObject staged = stagingArea.getOrCreateStagingTreeObject();

        if(staged.getContentAsObject().getPatches().isEmpty()) {
            // there are no staged changes to commit
            System.out.println("Nothing to commit");
            return ExitCode.ERROR;
        }

        Content content = new Content(staged.getContent());
        content.setAuthor(content.getAuthor());
        content.setMessage(message);
        content.setPatches(content.getPatches());
        staged.setContent(content.toBlob());
        byte[] bytes = staged.toDiskBytes();
        String hash = EncodingUtil.toHexString(EncodingUtil.hash(bytes));

        // find the current branch
        String branch = repository.getCurrentBranch();

        // update index
        content.getPatches() //
                .forEach(patch -> {
                    if(PathUtil.DEV_NULL.equals(patch.getTo())) {
                        index.removeFromIndex(patch.getTo());
                    } else {
                        index.putState(patch.getTo(), hash);
                    }
                });

        // update the reference on the branch
        FileUtil.writeContentToPath(FileUtil.findFileInDotTale(branch), hash);

        // write the new commit object
        FileUtil.writeContentToPath(FileUtil.findFileInDotTale("objects",
                hash.substring(0, 2), hash.substring(2)), bytes);

        // delete the staged file
        stagingArea.reset();

        return ExitCode.SUCCESS;
    }
}
