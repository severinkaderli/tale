package dev.tale.cli.implementation;

import dev.tale.cli.command.ExitCode;
import dev.tale.cli.config.Config;
import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.patch.Patch;
import dev.tale.cli.patch.PatchGenerator;
import dev.tale.cli.repository.Index;
import dev.tale.cli.repository.StagingArea;
import dev.tale.cli.repository.object.Content;
import dev.tale.cli.repository.object.TreeObject;
import dev.tale.cli.util.FileUtil;
import dev.tale.cli.util.PathUtil;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class DeleteImpl implements ServiceProvider<DeleteImpl> {
    @Inject
    private StagingArea stagingArea;

    @Inject
    private Index index;

    @Override
    public void init() {

    }

    @Override
    public DeleteImpl getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[]{
                StagingArea.class,
                Index.class
        };
    }

    public Integer delete(Path... paths) {
        TreeObject stagedObject = stagingArea.getOrCreateStagingTreeObject();
        Content stagedContent = new Content(stagedObject.getContent());

        List<Path> relativizedPaths = Arrays.stream(paths).map(PathUtil::relativize).collect(Collectors.toList());

        List<String> message = new ArrayList<>();

        for(Path relative : relativizedPaths) {
            List<String> currentState = FileUtil.loadFileFromPath(relative);
            Patch patch = PatchGenerator.generate(relative, PathUtil.DEV_NULL, currentState, new LinkedList<>());
            stagedContent.addPatch(patch);
            FileUtil.remove(relative.toFile());
            message.add("Deleted " + relative.toString());
        }

        String msg = String.join("\n", message);

        System.out.println(msg);


        stagedContent.setAuthor(Config.getInstance().getUser());
        stagedContent.setMessage(msg);
        stagedObject.setContent(stagedContent.toBlob());
        stagedObject.updateTimestamp();
        stagingArea.updateStagingTreeObject(stagedObject);
        String stagedId = stagingArea.getStagedId();

        for(Path relative : relativizedPaths) {
            index.putState(relative, stagedId);
        }


        return ExitCode.SUCCESS;
    }
}
