package dev.tale.cli.command;

import dev.tale.cli.patch.Patch;
import dev.tale.cli.patch.PatchGenerator;
import dev.tale.cli.util.PathUtil;
import picocli.CommandLine;

import java.nio.file.Path;
import java.util.Objects;

@CommandLine.Command(name = "--no-index", description = "Shows changes between two files.")
public class NoIndexDiffCommand extends Subcommand {

    @CommandLine.Parameters(index = "0", paramLabel = "<path>")
    Path fileA;

    @CommandLine.Parameters(index = "1", paramLabel = "<path>")
    Path fileB;

    @Override
    public Integer call() throws Exception {
        if(Objects.nonNull(fileA) && Objects.nonNull(fileB)) {
            return this.noIndexDiff();
        }

        CommandLine.usage(this, System.out);
        return ExitCode.USAGE;
    }

    private Integer noIndexDiff() {
        Path aPath = PathUtil.relativize(this.fileA);
        Path bPath = PathUtil.relativize(this.fileB);

        Patch diff = PatchGenerator.generate(aPath, bPath);
        System.out.print(diff.toCLIRepresentation());

        return ExitCode.SUCCESS;
    }
}
