package dev.tale.cli.command;

import dev.tale.cli.di.ServiceFactory;
import dev.tale.cli.implementation.PatchImpl;
import dev.tale.cli.util.RepositoryUtil;
import picocli.CommandLine;

import java.nio.file.Path;
import java.util.Objects;

@CommandLine.Command(
        name = "apply",
        description = "Apply a patch to files or the index.")
public class ApplyCommand extends Subcommand {
    @CommandLine.Parameters(index = "0", type = Path.class)
    private Path patchPath;

    @CommandLine.Parameters(index = "1..*", type = Path.class)
    private Path[] paths;

    @Override
    public Integer call() throws Exception {
        RepositoryUtil.assertRunningInRepository();
        if(Objects.isNull(this.patchPath) || !this.patchPath.toFile().exists()) {
            CommandLine.usage(this, System.out);
            return ExitCode.USAGE;
        }

        PatchImpl impl = ServiceFactory.get(PatchImpl.class);

        if(Objects.nonNull(this.paths) && this.paths.length > 0) {
            return impl.patchFiles(this.patchPath, this.paths, isVerbose);
        } else {
            return impl.patchIndex(this.patchPath);
        }
    }
}
