package dev.tale.cli.command;

import dev.tale.cli.di.ServiceFactory;
import dev.tale.cli.implementation.AddImpl;
import dev.tale.cli.util.RepositoryUtil;
import picocli.CommandLine;

import java.nio.file.Path;

@CommandLine.Command(name = "add", description = "Adds the specified files to the staging area.")
public class AddCommand extends Subcommand {
    @CommandLine.Parameters(index = "0..*", type = Path.class)
    private Path[] paths;

    @Override
    public Integer call() {
        RepositoryUtil.assertRunningInRepository();
        if (this.paths != null) {
            AddImpl impl = ServiceFactory.get(AddImpl.class);
            return impl.noOptionsAdd(paths);
        }

        return ExitCode.USAGE;
    }

}
