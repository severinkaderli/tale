package dev.tale.cli.command;

import dev.tale.cli.implementation.Initializer;
import picocli.CommandLine;

import java.util.Optional;

@CommandLine.Command(name = "init", description = "Creates an empty tale repository")
public class InitCommand extends Subcommand {
    @Override
    public Integer call() {
        Initializer initializer = new Initializer();
        return initializer.initialize(Optional.empty());
    }
}
