package dev.tale.cli.command;

import dev.tale.cli.di.ServiceFactory;
import dev.tale.cli.implementation.ShowImpl;
import dev.tale.cli.util.RepositoryUtil;
import picocli.CommandLine;

@CommandLine.Command(name = "show", description = "Show the contents of a given commit")
public class ShowCommand extends Subcommand {

    @CommandLine.Parameters(defaultValue = "HEAD", arity = "0..1")
    private String ref;

    @Override
    public Integer call() {
        RepositoryUtil.assertRunningInRepository();
        ShowImpl impl = ServiceFactory.get(ShowImpl.class);
        return impl.showCommit(ref, isVerbose);
    }
}
