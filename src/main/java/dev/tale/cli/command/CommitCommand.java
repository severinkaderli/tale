package dev.tale.cli.command;

import dev.tale.cli.di.ServiceFactory;
import dev.tale.cli.implementation.CommitImpl;
import dev.tale.cli.util.RepositoryUtil;
import picocli.CommandLine;

@CommandLine.Command(name = "commit", description = "Record changes to the repository.")
public class CommitCommand extends Subcommand {
    @CommandLine.Option(names = {"-m", "--message"}, required = true)
    String msg;

    @Override
    public Integer call() {
        RepositoryUtil.assertRunningInRepository();
        CommitImpl impl = ServiceFactory.get(CommitImpl.class);
        return impl.commit(msg);
    }

}
