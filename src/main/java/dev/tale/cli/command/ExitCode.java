package dev.tale.cli.command;

public class ExitCode {
    public static final int SUCCESS = 0;
    public static final int ERROR = 1;
    public static final int USAGE = 2;
}
