package dev.tale.cli.command;

import dev.tale.cli.di.ServiceFactory;
import dev.tale.cli.implementation.LogImpl;
import dev.tale.cli.repository.Reference;
import dev.tale.cli.util.FileUtil;
import dev.tale.cli.util.RepositoryUtil;
import picocli.CommandLine;

@CommandLine.Command(name = "log", description = "Show commit logs.")
public class LogCommand extends Subcommand {
    @CommandLine.Option(names = {"--oneline"}, description = "Show each commit on one line.")
    public boolean isOneline = false;

    @Override
    public Integer call() {
        RepositoryUtil.assertRunningInRepository();
        String headId = Reference.resolve(FileUtil.findFileInDotTale("HEAD"));

        LogImpl impl = ServiceFactory.get(LogImpl.class);
        return impl.printHistory(headId, isOneline, isVerbose);
    }

}
