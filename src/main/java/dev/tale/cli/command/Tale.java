package dev.tale.cli.command;

import picocli.CommandLine;

import java.util.concurrent.Callable;

@CommandLine.Command(
        name = "tale",
        mixinStandardHelpOptions = true,
        version = "tale 0.1.0",
        description = "The version control system of the future.",
        subcommands = {
                CommandLine.HelpCommand.class,
                DiffCommand.class,
                InitCommand.class,
                AddCommand.class,
                ApplyCommand.class,
                StatusCommand.class,
                LogCommand.class,
                CommitCommand.class,
                ShowCommand.class,
                //MoveCommand.class,
                DeleteCommand.class,
                ConfigCommand.class,
                RevertCommand.class
        }
)
public class Tale implements Callable<Integer> {
    @Override
    public Integer call() {
        CommandLine.usage(this, System.out);
        return ExitCode.USAGE;
    }
}
