package dev.tale.cli.command;

import picocli.CommandLine;

import java.util.concurrent.Callable;

abstract class Subcommand implements Callable<Integer> {
    @CommandLine.Option(names = {"-v", "--verbose"}, description = "Show detailed output.")
    boolean isVerbose;
}
