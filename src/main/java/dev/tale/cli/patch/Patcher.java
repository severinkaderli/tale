package dev.tale.cli.patch;

import dev.tale.cli.exceptions.ApplicationException;

import java.util.List;

public class Patcher {
    private List<Hunk> hunks;

    public Patcher(List<Hunk> hunks) {
        hunks.sort(Hunk::compareTo);
        this.hunks = hunks;
    }

    /**
     * Applies this patch to the given data.
     */
    public List<String> applyTo(List<String> from) {
        for(Hunk hunk : hunks) {
            applyHunk(from, hunk);
        }
        return from;
    }

    private void applyHunk(List<String> target, Hunk hunk) {
        List<String> diff = hunk.getContent();
        int pos = hunk.getHeader().getFromLine();
        for(int i = 0; i < diff.size(); i++) {
            switch (diff.get(i).charAt(0)) {
                case Hunk.ADD:
                    target.add(pos - 1, diff.get(i).substring(1));
                    pos++;
                    break;
                case Hunk.REMOVE:
                    target.remove(pos - 1);
                    break;
                case Hunk.SAME:
                    pos++;
                    break;
                default:
                    throw new ApplicationException("Not a valid patch");
            }
        }
    }
}
