package dev.tale.cli.patch;

import dev.tale.cli.datastructures.Vertex;
import lombok.Value;

@Value
public class PatchLine {
    private final int fromLine;
    private final int toLine;
    private final String content;
    private final boolean same;

    public PatchLine(Vertex pos, String content) {
        this(pos, content, false);
    }

    public PatchLine(Vertex pos, String content, boolean same) {
        this.fromLine = pos.x;
        this.toLine = pos.y;
        this.content = content;
        this.same = same;
    }

    @Override
    public String toString() {
        return String.format("%s, [%d, %d]", content, fromLine, toLine);
    }
}
