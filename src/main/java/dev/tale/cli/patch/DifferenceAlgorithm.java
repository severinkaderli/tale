package dev.tale.cli.patch;

import dev.tale.cli.datastructures.Vertex;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This class implements diffing based on Eugene W. Myers paper
 * "An O(ND) Difference Algorithm and Its Variations"
 * https://doi.org/10.1007/BF01840446
 * <p>
 * It uses the terms described in the paper in the core algorithm.
 */
public class DifferenceAlgorithm {
    private final List<String> from;
    private final List<String> to;
    private final int N;
    private final int M;

    public DifferenceAlgorithm(final List<String> from, final List<String> to) {
        this.from = from;
        this.to = to;
        this.N = this.from.size();
        this.M = this.to.size();
    }

    /**
     * @return a reverse EditPath from the given Lists {@code from} and {@code to}.
     */
    public Stack<Vertex> run() {
        // holds the furthest reaching paths per diagonal k for every D
        List<Map<Integer, Integer>> furthestReachingPaths = solveForward();

        Stack<Vertex> initialPath = new Stack<>();
        initialPath.push(new Vertex(N, M)); // start at the endpoint

        return solveBackward(furthestReachingPaths, furthestReachingPaths.size() - 1, initialPath);
    }


    private List<Map<Integer, Integer>> solveForward() {
        // holds the endpoints in terms of k, where k is the diagonal defined as k = x - y
        final Map<Integer, Integer> V = new HashMap<>();

        final int MAX = M + N;

        final List<Map<Integer, Integer>> endpoints = new ArrayList<>(MAX);

        V.put(1, 0);

        for (int D = 0; D <= MAX; D++) {
            for (int k = -D; k <= D; k += 2) {
                int x;
                if (k == -D || k != D && V.computeIfAbsent(k - 1, unused -> 0) < V.computeIfAbsent(k + 1, unused -> 0)) {
                    x = V.computeIfAbsent(k + 1, unused -> 0);
                } else {
                    x = V.computeIfAbsent(k - 1, unused -> 0) + 1;
                }
                int y = x - k;

                while (x < N && y < M && from.get(x).equals(to.get(y))) {
                    x++;
                    y++;
                }
                V.put(k, x);

                if (x >= N && y >= M) {
                    return endpoints;
                }
            }
            endpoints.add(clone(V));
        }

        throw new IllegalStateException("Couldn't calculate diff. Length of SES is greater than M + N");
    }

    /**
     * Shallow-Clones the given map.
     */
    private Map<Integer, Integer> clone(Map<Integer, Integer> o) {
        return o.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private Stack<Vertex> solveBackward(List<Map<Integer, Integer>> endpoints, int d, Stack<Vertex> path) {
        Vertex current = path.peek();

        // start reached
        if (current.x == 0 && current.y == 0) {
            return path;
        }

        // left edge reached, move to start
        if (current.x == 0) {
            while (path.peek().y > 0) {
                path.push(new Vertex(0, path.peek().y - 1));
            }
            return path;
        }

        // top edge reached, move to start
        if (current.y == 0) {
            while (path.peek().x > 0) {
                path.push(new Vertex(path.peek().x - 1, 0));
            }
            return path;
        }

        // diagonal of the current vertex
        int k = current.x - current.y;

        if (d == -1) { // no more paths to explore
            if (from.get(current.x - 1).equals(to.get(current.y - 1))) { // on a snake
                path.push(new Vertex(current.x - 1, current.y - 1));
                return solveBackward(endpoints, d, path);
            }
            return path;
        }

        Map<Integer, Integer> Vd = endpoints.get(d);

        if (Vd.containsKey(k + 1) && Vd.get(k + 1) == current.x) { // vertical extension
            path.push(new Vertex(current.x, current.y - 1));
            return solveBackward(endpoints, d - 1, path);
        } else if (Vd.containsKey(k - 1) && (Vd.get(k - 1) - (k - 1)) == current.y) { // horizontal extension
            path.push(new Vertex(current.x - 1, current.y));
            return solveBackward(endpoints, d - 1, path);
        } else {
            if (from.get(current.x - 1).equals(to.get(current.y - 1))) { // on a snake
                path.push(new Vertex(current.x - 1, current.y - 1));
                return solveBackward(endpoints, d, path);
            }
        }

        throw new IllegalStateException("Couldn't backtrack a path to 0,0. Current path = " + path);
    }
}
