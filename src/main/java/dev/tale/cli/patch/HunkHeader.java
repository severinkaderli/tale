package dev.tale.cli.patch;

import lombok.Builder;
import lombok.Value;

import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Value
@Builder
public class HunkHeader implements Comparable<HunkHeader>, CLISerializable, Reversible<HunkHeader> {
    private static final Pattern HUNK_REGEX = Pattern.compile("@@ -(\\d+),(\\d+) \\+(\\d+),(\\d+) @@((\\\\r)?\\\\n)?");
    private int fromLine;
    private int fromLength;
    private int toLine;
    private int toLength;

    public static HunkHeader fromString(final String input) {
        Matcher matcher = HUNK_REGEX.matcher(input);
        if (matcher.matches()) {
            return HunkHeader.builder()
                    .fromLine(extractGroup(matcher, 1)) //
                    .fromLength(extractGroup(matcher, 2)) //
                    .toLine(extractGroup(matcher, 3)) //
                    .toLength(extractGroup(matcher, 4)) //
                    .build();
        } else {
            throw new IllegalArgumentException(String.format("\"%s\" is not a valid hunk.", input));
        }
    }

    private static int extractGroup(Matcher matcher, int group) {
        return Integer.parseInt(matcher.group(group));
    }

    public static HunkHeader createHeaderFromContent(LinkedList<PatchLine> content) {
        PatchLine first = content.getFirst();
        PatchLine last = content.getLast();

        int firstFromLine = Math.max(first.getFromLine(), 1);
        int firstToLine = Math.max(first.getToLine(), 1);

        int fromLen = 1 + (last.getFromLine() - firstFromLine);
        int toLen = 1 + (last.getToLine() - firstToLine);

        return new HunkHeader(firstFromLine, fromLen, firstToLine, toLen);
    }

    @Override
    public int compareTo(HunkHeader that) {
        int compareFrom = Integer.compare(this.fromLine, that.fromLine);
        return -1 * (compareFrom != 0
                ? compareFrom
                : Integer.compare(this.toLine, that.toLine));
    }

    @Override
    public String toString() {
        return String.format("@@ -%d,%d +%d,%d @@", //
                this.fromLine, this.fromLength, //
                this.toLine, this.toLength);
    }

    @Override
    public String toCLIRepresentation() {
        return this.toString() + System.lineSeparator();
    }

    @Override
    public HunkHeader reverse() {
        HunkHeader.HunkHeaderBuilder builder = HunkHeader.builder();
        builder.fromLine(this.toLine);
        builder.fromLength(this.toLength);
        builder.toLine(this.fromLine);
        builder.toLength(this.fromLength);
        return builder.build();
    }
}
