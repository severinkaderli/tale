package dev.tale.cli.exceptions;

public class NotInRepositoryException extends ApplicationException{
    public NotInRepositoryException(String message) {
        super(message);
    }

    public NotInRepositoryException(String message, Throwable cause) {
        super(message, cause);
    }
}
