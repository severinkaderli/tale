package dev.tale.cli;

import dev.tale.cli.command.CommandFactory;

import java.util.Arrays;

public class Application {
    public static void main(String[] args) throws Exception{
        String command = args[0];
        String[] arguments = Arrays.copyOfRange(args, 1, args.length);
        CommandFactory.getInstance().createCommand(command, arguments).execute();
    }
}

