package fibsandlies;
import java.util.HashMap;

public class FibCalculator extends Fibonacci implements Calculator {

    private static Map<Integer, Integer> memoized = new HashMap<Integer, Integer>();

    public static int fibonacci(int fibIndex) {
        System.out.println(fibIndex);
        if (memoized.containsKey(fibIndex)) {
            int result = memoized.get(fibIndex);
            System.out.println(result);
            return result;
        } else {
            int answer = fibonacci(fibIndex - 1) + fibonacci(fibIndex - 2);
            System.out.println(answer);
            memoized.put(fibIndex, answer);
            return answer;
        }
    }
} //changing lines
// useless comment
