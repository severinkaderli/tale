<?php
declare(strict_types=1);

use App\Domain\Model\Product;
use App\Domain\Repository\ProductRepository;
use Firebase\JWT\JWT;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;

function jsonResponse(Response $response, $data)
{
  $response = $response->withHeader('Content-Type', 'application/json');
  $response->getBody()->write(json_encode($data));
  return $response;
}

return function (App $app) {
  $container = $app->getContainer();
  $jwt_secret = $container->get('settings')['jwt']['secret'];

  $app->get('/', function (Request $request, Response $response) {
    $response->getBody()->write('Hello world!');
    return $response;
  });

  // Products
  $app->get('/products', function (Request $request, Response $response) {
    $repository = new ProductRepository();

    $product = new Product();
    $product->setId(1);
    $product->setName("WD Green");
    $product->setPrice(7000);
    $product2 = new Product();
    $product2->setId(2);
    $product2->setName("Dstealth22");
    $product2->setPrice(133750);

    $data = [
      $product,
      $product2
    ];
    return jsonResponse($response, $data);
  });

  $app->get('/products/{id}', function (Request $request, Response $response, array $arguments) {
    $repository = new ProductRepository();
    return jsonResponse($response, $repository->findById(intval($arguments['id'])));
  });


  $app->post('/login', function (Request $request, Response $response) use ($jwt_secret) {
    $token = JWT::encode(['data' => 1], $jwt_secret, "HS256");
    return jsonResponse($response, ['token' => $token]);
  });
};

