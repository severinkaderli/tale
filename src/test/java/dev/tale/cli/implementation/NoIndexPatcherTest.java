package dev.tale.cli.implementation;

import dev.tale.cli.BaseTestCase;
import dev.tale.cli.util.FileUtil;
import dev.tale.cli.util.PathUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class NoIndexPatcherTest extends BaseTestCase {

    private static final Path LAO = Paths.get("lao");
    private static final Path TZU = Paths.get("tzu");

    @BeforeEach
    public void copyRelevantFiles() throws IOException {
        Files.copy(Paths.get("src/test/resources/diff/lao"), LAO);
        Files.copy(Paths.get("src/test/resources/diff/tzu"), TZU);
    }

    @Test
    public void shouldPatchLaoSuccessfullyToBeTzu() {
        PatchImpl patcher = new PatchImpl();
        Path patch = PathUtil.relativize(Paths.get("src/test/resources/patch/tzu.patch"));

        Map<Path, AtomicInteger> stats = patcher.patch(patch, LAO);

        assertNotNull(stats.get(LAO));
        assertEquals(1, stats.get(LAO).intValue());
        assertThat(FileUtil.loadFileFromPath(LAO), equalTo(FileUtil.loadFileFromPath(TZU)));
    }

    @AfterEach
    public void cleanUp() throws IOException{
        Files.delete(LAO);
        Files.delete(TZU);
    }
}
