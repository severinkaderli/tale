package dev.tale.cli.implementation;

import dev.tale.cli.BaseTestCase;
import dev.tale.cli.command.ExitCode;
import dev.tale.cli.di.ServiceFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

public class PatcherImplTest extends BaseTestCase {
    private ByteArrayOutputStream stdOut = new ByteArrayOutputStream();
    private ByteArrayOutputStream stdErr = new ByteArrayOutputStream();

    @BeforeEach
    public void beforeEach() {
        ServiceFactory.reset();
        System.setOut(new PrintStream(stdOut));
        System.setErr(new PrintStream(stdErr));

        stdOut.reset();
        stdErr.reset();
    }

    @Test
    public void testStatistics() {
        PatchImpl impl = new PatchImpl();
        impl = spy(impl);

        Map<Path, AtomicInteger> stats = new HashMap<>();
        stats.put(Paths.get("B"), new AtomicInteger(2));
        doReturn(stats).when(impl).patch(any(Path.class), anyVararg());

        assertEquals(ExitCode.SUCCESS, impl.patchFiles(Paths.get("A"), new Path[] {Paths.get("B")}, false));

        assertThat("Expected silence", stdOut.toString(), is(equalTo("")));
    }

    @Test
    public void testStatisticsVerbose() {
        PatchImpl impl = new PatchImpl();
        impl = spy(impl);

        Map<Path, AtomicInteger> stats = new HashMap<>();
        stats.put(Paths.get("B"), new AtomicInteger(2));

        doReturn(stats).when(impl).patch(any(Path.class), anyVararg());

        assertEquals(ExitCode.SUCCESS, impl.patchFiles(Paths.get("A"), new Path[] {Paths.get("B")}, true));

        assertThat("Expected statistics", stdOut.toString(),
                is(equalTo("Successfully applied the following patches:\nPatches  Path\n      2  B\n")));
    }
}
