package dev.tale.cli.implementation;

import dev.tale.cli.command.ExitCode;
import dev.tale.cli.config.Config;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.io.FileMatchers.anExistingDirectory;

public class InitializerTest {

    private Initializer initializer;

    @BeforeEach
    public void setUp() {
        initializer = new Initializer();
    }

    @Test
    public void shouldInitEmptyRepository() {
        int exitCode = initializer.initialize();
        assertThat(exitCode, equalTo(ExitCode.SUCCESS));

        File dotTale = Paths.get(".tale").toFile();
        assertThat(dotTale, is(anExistingDirectory()));
        String[] taleChildren = dotTale.list();
        assertThat(taleChildren, is(not(nullValue())));
        List<String> taleContents = Arrays.asList(taleChildren);
        assertThat(taleContents, containsInAnyOrder(
                "branches",
                "HEAD",
                "staged",
                "objects",
                "config.toml",
                "index"
        ));


        Config config = Config.getInstance();
        assertThat("Config file not initalized", config.getUser(), equalTo(System.getProperty("user.name")));
        assertThat("Config file not initalized", config.getDateFormat(), equalTo("yyyy-MM-dd HH:mm:ss"));

        exitCode = initializer.initialize();
        assertThat(exitCode, equalTo(ExitCode.SUCCESS));

        assertThat("Unable to cleanup after test", delete(dotTale), is(true));
    }

    @Test
    public void shouldExitWithErrorWhenInvalidDotTaleExists() throws Exception {
        File dotTale = Paths.get(".tale").toFile();
        dotTale.createNewFile();
        assertThat(initializer.initialize(), is(ExitCode.ERROR));
    }



    private boolean delete(File f) {
        boolean successfull = true;
        if (f.isDirectory()) {
            successfull = Arrays.stream(f.list()).allMatch(c -> delete(new File(f, c)));
        }
        successfull &= f.delete();
        return successfull;
    }

    @AfterEach
    public void tearDown() {
        File dotTale = Paths.get(".tale").toFile();
        this.delete(dotTale);
    }
}
