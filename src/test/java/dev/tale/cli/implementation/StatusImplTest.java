package dev.tale.cli.implementation;

import dev.tale.cli.BaseTestCase;
import dev.tale.cli.command.ExitCode;
import dev.tale.cli.datastructures.Pair;
import dev.tale.cli.di.ServiceFactory;
import dev.tale.cli.repository.Repository;
import dev.tale.cli.util.PathUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StatusImplTest extends BaseTestCase {
    private ByteArrayOutputStream stdOut = new ByteArrayOutputStream();
    private ByteArrayOutputStream stdErr = new ByteArrayOutputStream();

    @BeforeEach
    public void beforeEach() {
        ServiceFactory.reset();
        System.setOut(new PrintStream(stdOut));
        System.setErr(new PrintStream(stdErr));

        stdOut.reset();
        stdErr.reset();
    }

    @Test
    public void when_newFile_shouldDisplayUntracked() {
        assertStatus(
                "Untracked Files:\n\tHello.java",
                Collections.singletonList(new Pair<>(PathUtil.DEV_NULL, Paths.get("Hello.java"))),
                Collections.emptyList(),
                Collections.emptyList()
        );
    }

    @Test
    public void when_stagedFile_shouldDisplayStaged() {
        assertStatus(
                "Changes to be commited:\n\tnew file:\tHello.java",
                Collections.emptyList(),
                Collections.singletonList(new Pair<>(PathUtil.DEV_NULL, Paths.get("Hello.java"))),
                Collections.emptyList()
        );
    }

    @Test
    public void when_stagedDeletion_shouldDisplayStaged() {
        assertStatus(
                "Changes to be commited:\n\tdeleted:\tHello.java",
                Collections.emptyList(),
                Collections.singletonList(new Pair<>(Paths.get("Hello.java"), PathUtil.DEV_NULL)),
                Collections.emptyList()
        );
    }

    @Test
    public void when_stagedMove_shouldDisplayStaged() {
        assertStatus(
                "Changes to be commited:\n\trenamed:\tHello.java -> World.java",
                Collections.emptyList(),
                Collections.singletonList(new Pair<>(Paths.get("Hello.java"), Paths.get("World.java"))),
                Collections.emptyList()
        );
    }

    @Test
    public void when_stagedModified_shouldDisplayStaged() {
        assertStatus(
                "Changes to be commited:\n\tmodified:\tHello.java",
                Collections.emptyList(),
                Collections.singletonList(new Pair<>(Paths.get("Hello.java"), Paths.get("Hello.java"))),
                Collections.emptyList()
        );
    }

    @Test
    public void when_changedFile_shouldDisplayChanged() {
        assertStatus(
                "Changes not staged for commit:\n\tmodified:\tHello.java",
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.singletonList(new Pair<>(PathUtil.DEV_NULL, Paths.get("Hello.java")))
        );
    }

    @Test
    public void when_changedAndNewFiles_shouldDisplayCorrectly() {
        assertStatus(
                "Changes not staged for commit:\n\tmodified:\tHello.java\nUntracked Files:\n\tWorld.java",
                Collections.singletonList(new Pair<>(PathUtil.DEV_NULL, Paths.get("World.java"))),
                Collections.emptyList(),
                Collections.singletonList(new Pair<>(PathUtil.DEV_NULL, Paths.get("Hello.java")))
        );
    }

    private void assertStatus(String expectedOutput, Collection<Pair<Path, Path>> untracked,
                              Collection<Pair<Path, Path>> staged,
                              Collection<Pair<Path, Path>> changed) {
        Repository repositoryMock = mock(Repository.class);
        when(repositoryMock.listUntrackedFiles()).thenReturn(untracked);
        when(repositoryMock.listStagedFiles()).thenReturn(staged);
        when(repositoryMock.listChangedFiles()).thenReturn(changed);

        ServiceFactory.putMock(Repository.class, repositoryMock);
        StatusImpl impl = ServiceFactory.get(StatusImpl.class);

        assertEquals(ExitCode.SUCCESS, impl.printStatus());
        assertThat("Did not get the expected output.", stdOut.toString().trim(), equalTo(expectedOutput));
        assertThat("Unexpected output to Std::ERR", stdErr.size(), equalTo(0));
    }
}
