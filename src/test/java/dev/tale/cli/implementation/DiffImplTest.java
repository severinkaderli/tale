package dev.tale.cli.implementation;

import dev.tale.cli.BaseTestCase;
import dev.tale.cli.command.ExitCode;
import dev.tale.cli.di.ServiceFactory;
import dev.tale.cli.repository.History;
import dev.tale.cli.repository.Index;
import dev.tale.cli.util.FileUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DiffImplTest extends BaseTestCase {
    private ByteArrayOutputStream stdOut = new ByteArrayOutputStream();
    private ByteArrayOutputStream stdErr = new ByteArrayOutputStream();

    @BeforeEach
    public void beforeEach() {
        ServiceFactory.reset();
        System.setOut(new PrintStream(stdOut));
        System.setErr(new PrintStream(stdErr));

        stdOut.reset();
        stdErr.reset();
    }

    private static Path aPath = Paths.get("A");
    private static String id = "abcdef";

    @AfterAll
    public static void cleanUp() {
        FileUtil.remove(aPath.toFile());
    }

    @Test
    public void testSimpleDiff() {
        FileUtil.writeContentToPath(aPath, "one\ntwo\nthree\n");

        Index indexMock = mock(Index.class);
        when(indexMock.getState(eq(aPath))).thenReturn(id);

        History historyMock = mock(History.class);
        when(historyMock.getHistoricState(eq(aPath), eq(id))).thenReturn(Arrays.asList("one\\n", "zwei\\n"));

        ServiceFactory.putMock(Index.class, indexMock);
        ServiceFactory.putMock(History.class, historyMock);

        DiffImpl impl = ServiceFactory.get(DiffImpl.class);

        assertEquals(ExitCode.SUCCESS, impl.indexDiff(new Path[]{aPath}));
        String expected = "\u001B[1m--- A\n" +
                "+++ A\n" +
                "\u001B[21m\u001B[0m\u001B[34m@@ -1,2 +1,3 @@\n" +
                "\u001B[39m\u001B[0m one\n" +
                "\u001B[31m-zwei\n" +
                "\u001B[39m\u001B[0m\u001B[32m+two\n" +
                "\u001B[39m\u001B[0m\u001B[32m+three\n" +
                "\u001B[39m\u001B[0m\n";
        assertEquals(expected, stdOut.toString());
    }
}
