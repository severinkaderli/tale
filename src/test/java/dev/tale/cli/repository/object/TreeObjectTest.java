package dev.tale.cli.repository.object;

import dev.tale.cli.BaseTestCase;
import dev.tale.cli.patch.Hunk;
import dev.tale.cli.patch.HunkHeader;
import dev.tale.cli.patch.Patch;
import dev.tale.cli.util.FileUtil;
import dev.tale.cli.util.PathUtil;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class TreeObjectTest extends BaseTestCase {
    @Test
    public void test_serializeMinimalTreeObject() throws Exception {
        File f = FileUtil.getResourcePath("minimalObject").toFile();
        TreeObject object = TreeObject.read(f);
        assertArrayEquals(Files.readAllBytes(f.toPath()), object.toDiskBytes());
        assertEquals("b2c003ed6eb922afcf1deb39df590e0a6387acfd25303d4c8dd3046fce6af926", object.hash());
    }

    @Test
    public void test_deserializeMinimalTreeObjectContent() {
        File f = FileUtil.getResourcePath("minimalObject").toFile();
        TreeObject object = TreeObject.read(f);
        Content content = new Content(object.getContent());
        assertThat(content.getAuthor(), is(equalTo("testuser")));
        assertThat(content.getMessage(), is(equalTo("Commitmessage")));
    }

    @Test
    public void test_deserializeTreeObjectContentWithDiffs() {
        Hunk h1 = new Hunk(HunkHeader.fromString("@@ -9,3 +8,6 @@"));
        h1.addContent(" The two are the same,\\n");
        h1.addContent(" But after they are produced,\\n");
        h1.addContent(" they have different names.\\n");
        h1.addContent("+They both may be called deep and profound.\\n");
        h1.addContent("+Deeper and more profound,\\n");
        h1.addContent("+The door of all subtleties!\\n");

        Hunk h2 = new Hunk(HunkHeader.fromString("@@ -1,7 +1,6 @@"));
        h2.addContent("-The Way that can be told of is not the eternal Way;\\n");
        h2.addContent("-The name that can be named is not the eternal name.\\n");
        h2.addContent("The Nameless is the origin of Heaven and Earth;\\n");
        h2.addContent("-The Named is the mother of all things.\\n");
        h2.addContent("+The named is the mother of all things.\\n");
        h2.addContent("+\\n");
        h2.addContent("Therefore let there always be non-being,\\n");
        h2.addContent("so we may see their subtlety,\\n");
        h2.addContent("And let there always be being,\\n");


        Patch patch = Patch.builder() //
                .from(PathUtil.relativize(Paths.get("src/test/resources/lao"))) //
                .to(PathUtil.relativize(Paths.get("src/test/resources/tzu"))) //
                .hunks(Arrays.asList(h1, h2)) //
                .build();

        Content content = Content.builder() //
                .author("testuser") //
                .message("Commit Message Example") //
                .patches(Collections.singletonList(patch)) //
                .build();

        TreeObject treeObject = TreeObject.builder() //
                .parentID("abcdef") //
                .parent2ID("1234567890") //
                .timestamp(System.currentTimeMillis()) //
                .content(content.toBlob()) //
                .build();

        Path tmpFile = Paths.get("tmp");

        FileUtil.writeContentToPath(tmpFile, treeObject.toDiskBytes());

        TreeObject diskTreeObject = TreeObject.read(tmpFile.toFile());

        assertEquals(treeObject.getParentID(), diskTreeObject.getParentID());
        assertEquals(treeObject.getParent2ID(), diskTreeObject.getParent2ID());

        Content diskContent = new Content(diskTreeObject.getContent());
        assertEquals(content.getAuthor(), diskContent.getAuthor());
        assertEquals(content.getMessage(), diskContent.getMessage());
        assertEquals(content.getPatches(), diskContent.getPatches());

        assertTrue(tmpFile.toFile().delete());
    }
}
