package dev.tale.cli.repository;

import dev.tale.cli.BaseTestCase;
import dev.tale.cli.exceptions.ApplicationException;
import dev.tale.cli.util.EncodingUtil;
import dev.tale.cli.util.FileUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ReferenceTest extends BaseTestCase {

    private static Set<Path> toDelete = new HashSet<>();

    @AfterEach
    public void tearDown() {
        super.tearDown();
        toDelete.forEach(p -> FileUtil.remove(p.toFile()));
    }

    @Test
    public void testToPath() {
        String id = EncodingUtil.hash("object#1");
        assertThat("Path not correctly mapped", Reference.toPath(id).toString(),
                endsWith(".tale/objects/" + id.substring(0, 2) + "/" + id.substring(2)));
    }

    @Test
    public void testReverseEmpty() {
        assertThat(Reference.reverse(new Stack<>()).size(), is(0));
    }

    @Test
    public void testReverse() {
        Stack<String> in = new Stack<>();
        in.push("1");
        in.push("2");
        in.push("3");

        assertThat(Reference.reverse(in), contains("3", "2", "1"));
    }

    @Test
    public void testPartialResolveTooShort() {
        ApplicationException exception = assertThrows(ApplicationException.class, () -> {
            Reference.resolvePartial("123");
        });
        assertThat("Wrong exception message",
                exception.getMessage(),
                startsWith("Cannot resolve object-refs shorter than"));
    }

    @Test
    public void testPartialResolveNotFound() {
        ApplicationException exception = assertThrows(ApplicationException.class, () -> {
            Reference.resolvePartial("ffffff");
        });
        assertThat("Wrong exception message",
                exception.getMessage(),
                startsWith("ffffff is not a known object-ref."));
    }

    @Test
    public void testPartialResolveAmbigous() {
        writeObject(pad(""), pad("abcd1"));
        writeObject(pad(""), pad("abcd2"));
        ApplicationException exception = assertThrows(ApplicationException.class, () -> {
            Reference.resolvePartial("abcd");
        });
        assertThat("Wrong exception message",
                exception.getMessage(),
                startsWith("abcd is an ambiguous object-ref."));
    }

    @Test
    public void testPartialResolveHappyCase() {
        Path abcd1 = writeObject(pad(""), pad("abcd1"));
        Path abcd2 = writeObject(pad(""), pad("abcd2"));

        assertThat("Wrong path returned", Reference.resolvePartial("abcd1").toAbsolutePath(), is(equalTo(abcd1)));
    }

    @Test
    public void testResolve() {
        Path head = FileUtil.findFileInDotTale("HEAD");
        toDelete.add(head);
        FileUtil.writeContentToPath(head, "branches/master");
        Path master = FileUtil.findFileInDotTale("branches", "master");
        toDelete.add(master);
        FileUtil.writeContentToPath(master, pad("abcdef"));

        writeObject(pad("abcdef"), pad("123456"));

        assertThat("Wrong resolution", Reference.resolve(head), is(equalTo(pad("abcdef"))));
    }

    @Test
    public void testResolveHierarchy() {
        String a = pad("a");
        String b = pad("b");
        String c = pad("c");
        writeObject(pad(""), a); // first commit
        writeObject(a, b); // second commit
        writeObject(b, c); // third commit
        Path head = FileUtil.findFileInDotTale("HEAD");
        toDelete.add(head);
        FileUtil.writeContentToPath(head, c);

        Stack<String> ids = Reference.resolveHierarchy(head, new Stack<>());
        assertThat("Wrong hierarchy", ids, contains(c, b, a));
    }

    private String pad(String id) {
        final String nullRef = "0000000000000000000000000000000000000000000000000000000000000000";
        return id + nullRef.substring(id.length());
    }

    private Path writeObject(String content, String id) {
        Path path = Reference.toPath(id);
        toDelete.add(path);
        FileUtil.writeContentToPath(path, content);
        return path;
    }
}
