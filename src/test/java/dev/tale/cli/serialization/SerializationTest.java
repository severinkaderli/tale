package dev.tale.cli.serialization;

import dev.tale.cli.BaseTestCase;
import dev.tale.cli.patch.Hunk;
import dev.tale.cli.patch.HunkHeader;
import dev.tale.cli.patch.Patch;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class SerializationTest extends BaseTestCase {
    @Test
    public void testHunkSerialization() {
        Hunk hunk = createHunk();
        String json = Serialization.gson().toJson(hunk);
        Hunk actual = Serialization.gson().fromJson(json, Hunk.class);

        assertThat(actual, is(equalTo(hunk)));
    }

    @Test
    public void testPathSerialization() {
        Path expected = Paths.get("src", "main", "java");
        String json = Serialization.gson().toJson(expected, Path.class);
        Path actual = Serialization.gson().fromJson(json, Path.class);

        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void testDifferenceSerialization() {
        Hunk hunk = createHunk();
        Path from = Paths.get("src", "main");
        Path to = Paths.get("src", "test");

        Patch expected = Patch.builder() //
                .from(from) //
                .to(to) //
                .hunks(Collections.singletonList(hunk)) //
                .build();

        String output = Serialization.difference(expected);
        Patch actual = Serialization.difference(output);

        assertThat(actual, is(equalTo(expected)));
    }

    private Hunk createHunk() {
        HunkHeader header = HunkHeader.builder() //
                .fromLine(1).fromLength(4) //
                .toLine(1).toLength(6).build();

        Hunk hunk = new Hunk(header);
        hunk.addContent("first line\r\n");
        hunk.addContent("second line\n");
        hunk.addContent("third line\n");

        return hunk;
    }
}
