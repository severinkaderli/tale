package dev.tale.cli.config;

import dev.tale.cli.BaseTestCase;
import dev.tale.cli.exceptions.ApplicationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ConfigTest extends BaseTestCase {

    @BeforeEach
    public void resetSingleton() throws NoSuchFieldException, IllegalAccessException {
        Field field = Config.class.getDeclaredField("INSTANCE");
        field.setAccessible(true);
        field.set(null, null);
    }

    @Test
    public void testConfigInitilaizationWithNoConfigFile() {
        assertTrue(this.removeTaleFolder());
        assertThrows(ApplicationException.class, () -> {
            Config config = Config.getInstance();
        });
    }
}