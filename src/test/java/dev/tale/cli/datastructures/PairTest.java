package dev.tale.cli.datastructures;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PairTest {
    @Test
    public void testSame() {
        assertComparison(1, 2, 1, 2, 0);
    }

    @Test
    public void testNull() {
        assertComparision(new Pair<>(1, 2), null, 0);
    }

    @Test
    public void testFirstSmaller() {
        assertComparison(1, 1, 1, 2, -1);
    }

    @Test
    public void testSecondSmaller() {
        assertComparison(2, 3, 1, 1, 1);
    }

    private void assertComparison(Integer a, Integer b, Integer c, Integer d, int result) {
        Pair<Integer, Integer> first = new Pair<>(a, b);
        Pair<Integer, Integer> second = new Pair<>(c, d);
        assertComparision(first, second, result);
    }

    private void assertComparision(Pair<Integer, Integer> first, Pair<Integer, Integer> second, int result) {

        Assertions.assertEquals(result, first.compareTo(second));
    }
}
