package dev.tale.cli.datastructures;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class SizeLimitedCacheTest {
    @Test
    public void testDefaultSizeLimit() {
        SizeLimitedCache<Integer, String> cache = new SizeLimitedCache<>();
        for(int i = 0; i < 65; i++) {
            cache.put(i, Integer.toHexString(i));
        }
        assertThat("Entry should not be in cache anymore", cache.get(0), is(nullValue()));
        assertThat("Entry should still be in cache", cache.get(1), is(equalTo("1")));
    }

    @Test
    public void testCustomSizeLimit() {
        SizeLimitedCache<Integer, String> cache = new SizeLimitedCache<>(2);
        for(int i = 0; i < 3; i++) {
            cache.put(i, Integer.toHexString(i));
        }
        assertThat("Entry should not be in cache anymore", cache.get(0), is(nullValue()));
        assertThat("Entry should still be in cache", cache.get(1), is(equalTo("1")));
    }

    @Test
    public void testComputeIfAbsent() {
        SizeLimitedCache<Integer, String> cache = new SizeLimitedCache<>();
        cache.computeIfAbsent(0, Integer::toHexString);
        cache.computeIfAbsent(0, k -> "Hello, World");
        assertThat("Entry should not be overwritten", cache.get(0), is(equalTo("0")));
    }
}
