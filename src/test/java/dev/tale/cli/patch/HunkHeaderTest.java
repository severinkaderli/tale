package dev.tale.cli.patch;

import dev.tale.cli.BaseTestCase;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class HunkHeaderTest extends BaseTestCase {
    @Test
    public void testHunkParsing() {
        String input = "@@ -1,28 +2,34 @@";
        HunkHeader actual = HunkHeader.fromString(input);
        assertEquals(1, actual.getFromLine());
        assertEquals(28, actual.getFromLength());
        assertEquals(2, actual.getToLine());
        assertEquals(34, actual.getToLength());
    }

    @Test
    public void testInvalidHunkParsing() {
        assertThrows(IllegalArgumentException.class, () -> {
            String input = "@ -1,28 +2, @@";
            HunkHeader actual = HunkHeader.fromString(input);
        });
    }

    @Test
    public void testHunkParsingWithNormalNewlines() {
        assertThrows(IllegalArgumentException.class, () -> {
            String input = "@ -1,28 +2, @@\\n";
            HunkHeader actual = HunkHeader.fromString(input);
        });
    }

    @Test
    public void testHunkParsingWithWindowsNewlines() {
        assertThrows(IllegalArgumentException.class, () -> {
            String input = "@ -1,28 +2, @@\\r\\n";
            HunkHeader actual = HunkHeader.fromString(input);
        });
    }
}
