package dev.tale.cli.command;

import dev.tale.cli.BaseTestCase;
import dev.tale.cli.util.FileUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import picocli.CommandLine;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;

class DiffCommandTest extends BaseTestCase {
    private DiffCommand diffCommand;

    @BeforeEach
    public void setUp() {
        super.setUp();
        diffCommand = new DiffCommand();
    }

    @Test
    public void shouldShowUsageIfNoArgumentsWereSupplied() {
        int exitCode = new CommandLine(diffCommand).execute();
        assertThat(exitCode, is(ExitCode.SUCCESS));
    }

    @Test
    public void shouldShowDiffAndExitCorrectly() {
        int exitCode = new CommandLine(diffCommand).execute(
                "--no-index",
                FileUtil.getResourcePath("diff/a.php").toString(),
                FileUtil.getResourcePath("diff/b.php").toString()
        );

        int numberOfLines = systemOut.toString().split("\r\n|\r|\n").length;

        assertThat(numberOfLines, is(greaterThan(40)));
        assertThat(exitCode, is(ExitCode.SUCCESS));
    }

    @Test
    public void shouldShowVerboseDiffAndExitCorrectly() {
        int exitCode = new CommandLine(diffCommand).execute(
                "-v",
                "--no-index",
                FileUtil.getResourcePath("diff/a.php").toString(),
                FileUtil.getResourcePath("diff/b.php").toString()
        );

        int numberOfLines = systemOut.toString().split("\r\n|\r|\n").length;

        assertThat(numberOfLines, is(equalTo(72)));
        assertThat(exitCode, is(ExitCode.SUCCESS));
    }
}