package dev.tale.cli.command;

import dev.tale.cli.BaseTestCase;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class TaleTest extends BaseTestCase {

    @Test
    public void testTaleCommand() {
        Tale tale = new Tale();
        assertThat("Tale doesn't exit with successful exit code", tale.call(), is(ExitCode.USAGE));
    }
}