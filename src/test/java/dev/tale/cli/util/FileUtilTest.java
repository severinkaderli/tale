package dev.tale.cli.util;

import dev.tale.cli.BaseTestCase;
import dev.tale.cli.exceptions.ApplicationException;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FileUtilTest extends BaseTestCase {

    @Test
    public void testFindDotTale() {
        File file = FileUtil.findDotTale(PathUtil.getInstance().getWorkingDir()).toFile();
        assertNotNull(file);

        file = FileUtil.findDotTale(Paths.get("./src").toFile());
        assertNotNull(file);

        file = FileUtil.findDotTale(Paths.get("../").toFile());
        assertNull(file);
    }

    @Test
    public void testFindFileInDotTale() {
        Path path = FileUtil.findFileInDotTale("config.toml");
        assertNotNull(path);
    }

    @Test
    public void testLoadFileFromPath() {
        assertThrows(ApplicationException.class, () -> FileUtil.loadFileFromPath(Paths.get("nope.jpg")));
    }


    @Test
    public void testFindFilesChangedSince() throws Exception {
        Path testDirectory = Paths.get("test-dir");
        Files.createDirectories(testDirectory);
        Path test1 = Files.createFile(Paths.get("test-dir/test1"));
        Path test2 = Files.createFile(Paths.get("test-dir/test2"));

        test1.toFile().setLastModified(500);
        test2.toFile().setLastModified(1000);

        List<Path> paths = FileUtil.findFilesChangedSince(Paths.get("test-dir"), 750);

        assertEquals(1, paths.size());

        FileUtil.remove(Paths.get("test-dir").toFile());
    }
}