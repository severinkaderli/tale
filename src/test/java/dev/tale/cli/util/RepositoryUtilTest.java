package dev.tale.cli.util;

import dev.tale.cli.BaseTestCase;
import dev.tale.cli.exceptions.ApplicationException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RepositoryUtilTest extends BaseTestCase {
    @Test
    public void testAssertRunningInRepository() {
        assertDoesNotThrow(RepositoryUtil::assertRunningInRepository);

        this.removeTaleFolder();

        assertThrows(ApplicationException.class, RepositoryUtil::assertRunningInRepository);
    }
}