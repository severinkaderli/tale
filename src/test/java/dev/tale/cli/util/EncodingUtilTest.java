package dev.tale.cli.util;

import dev.tale.cli.BaseTestCase;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class EncodingUtilTest extends BaseTestCase {
    @Test
    public void testStringHashing() {
        String input = "unittest";
        String expected = "df08227309c92f2f3f030b423009e697445371283d89352a26d064d39fb73c36";
        assertThat(EncodingUtil.hash(input), equalTo(expected));
    }

    @Test
    public void testBase64() {
        String base = "ZGVjb2RlIG1l";
        String plain = "decode me";
        assertThat(EncodingUtil.toBase64(plain), is(equalTo(base)));
        assertThat(EncodingUtil.fromBase64(base), is(equalTo(plain)));
    }
}
