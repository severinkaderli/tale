---
title: "tale"
subtitle: "Final Report"
author:
    - Marius Schär
    - Severin Kaderli
extra-info: true
institute: "Bern University of Applied Sciences"
department: "Engineering and Information Technology"
lecturer: "Joachim Wolfgang Kaltz"
lang: "en-UK"
toc: true
lot: true
rule-color: CDDC39
link-color: 448AFF
glossary: true
glossary-file: glossary.tex
...

# Initial Situation
As mentioned in the requirements specification document our goal with tale
was not to clone git, or build a git compatible version control system.
While we were inspired by git, because it's a tool we use daily,
we did not look at the internal workings of it before or during
the project.

The goal of this project was to build our own version control system,
with enough functionality to be usable.
While we did not implement all the planned features,
the features that are present are enough for tale to be useful.

This project was not a conventional software engineering project,
but more of a research and learning project.

# Baseline Plan
To build tale we decided to use a scrum inspired process.
We created user stories for each functional requirement and divided the work
into 5 sprints.
The original user stories can be found in the requirements specification document.
The original sprint plan can be seen on the next page.

Before we started with this sprint structure,
we focused on getting the core diffing algorithm right.
More information about the algorithm can be found in chapter \ref{patchgeneration}.

During Sprint 1 we realized that we would need `tale diff` (U3)
to debug commiting (U5).
Because of this we re-arranged the stories and focused first on
delivering `tale status` (U11) and `tale diff` (U3).

We decided during Sprint 2 that undoing changes (U9) was more important
than switching between commits at will (U6, U12).

During Sprint 3 we encountered various problems with software stability
because our path handling mechanic was not well conceived.
Additional problems were faced when using reflection for JSON deserialization
and configuration handling.
More information about reflection with GraalVM Native Images can
be found in chapter \ref{nativeimages}.

Because of these problems we experienced some delays, and had to cut
out some functional requirements.
More on this in chapter \ref{functionalreqs}.

We focused the end of sprint 4 and sprint 5 on improving stability and
polishing the features that we had started.

\newpage

\begin{tab}{5}{Sprint contents}{Sprint & Start Date & End Date & ID & Description}
  \textbf{Sprint 1} & 2019-10-21 & 2019-11-03 & U1 & Create a repository \\ \hline
  \multicolumn{3}{|c|}{} & U2 & Add contents \\ \hline                          
  \multicolumn{3}{|c|}{} & U5 & Record changes \\ \hline                        
  \multicolumn{3}{|c|}{} & U11 & Show status of repository \\ \hline            
  \textbf{Sprint 2} & 2019-11-04 & 2019-11-17 & U3 & Show differences \\ \hline 
  \multicolumn{3}{|c|}{} & U7 & Apply differences \\ \hline                     
  \multicolumn{3}{|c|}{} & U10 & Show history \\ \hline                         
  \multicolumn{3}{|c|}{} & U12 & Reset to specific commit \\ \hline             
  \textbf{Sprint 3} & 2019-11-18 & 2019-11-01 & U6 & Switch to a specific version/branch \\ \hline
  \multicolumn{3}{|c|}{} &  U9 & Undo commits \\ \hline                         
  \textbf{Sprint 4} & 2019-12-02 & 2019-12-15 & U4 & Create new branches \\ \hline
  \multicolumn{3}{|c|}{} &  U8 & Merge a branch into the current one \\ \hline  
  \textbf{Sprint 5} & 2019-12-02 & 2019-12-15 & & Cleanup and time reserves     
\end{tab}    

\newpage

# Analysis

## Function Requirements
\label{functionalreqs}
This chapter describes the planned requirements and whether we completed them or not.

Near the end of the project we elected not to press for new features
(P.8, P.9, P.10) but instead focus on stability and code quality.
Unfortunately this means that tale only supports linear history.

\begin{tab}{4}{Functional Requirements}{ID & Command & Description & Done}                      
 P.1   & `init`                        & Create an empty repository.                        & Yes \\ \hline
 P.2   &                               & Record changes in the repository.                  & Yes \\ \hline
 P.2.1 & `add <file>`                  & Add file contents to the index.                    & Yes \\ \hline
 P.2.2 & `commit`                      & Record changes to the repository with a message.   & Yes \\ \hline
 P.3   & `log`                         & Show past commits with different levels of detail. & Yes \\ \hline
 P.4   & `diff --no-index <a> <b>`     & Show difference between any two files.             & Yes \\ \hline
 P.5   & `diff`                        & Show difference between current changes and commit.& Yes \\ \hline
 P.6   & `revert <commit>`             & Undo a given commit.                               & Yes \\ \hline
 P.7   & `apply <diff>`                & Applies a given diff to the working tree.          & Yes \\ \hline
 P.8   & `branch <name>`               & Creates a new branch with the given name.          & No  \\ \hline
 P.9   & `checkout <branch or commit>` & Switches to a given branch or commit.              & No  \\ \hline
 P.10  & `merge <branch>`              & Merges the given branch into the current one.      & No 
\end{tab} 

\newpage

## Delivery Objects
All the delivery objects generated in this module can be found on our
[GitLab Page](https://gitlab.com/severinkaderli/tale).

The delivery objects are all built at each push using GitLab's CI feature.

\begin{tab}{3}{Delivery Objects}{Type & Name & Description}                      
  Document & Requirement Specification & Initial document which
  describes the requirement and planned process. \\ \hline
  Document & Final Report & Review and summary of the project,
  as well as some technical documentation \\ \hline
  Documentation & JavaDoc & Generated HTML of the JavaDoc \\ \hline
  Binary & tale & The compiled binary, runnable on most Linux systems.
  A GraalVM Native Image. \\ \hline
  Report & Java Code Coverage Report & Checks how much and what parts
  of our code is covered by automated tests.
\end{tab} 

Here is an example of the code coverage report badge:
![JaCoCo Badge](assets/codecov.png){height=1em}  
The full report can be found on [codecov.io](https://codecov.io/gl/severinkaderli/tale).

\newpage

## Technical Overview
This chapter contains a technical overview of the codebase.

### Component Overview
![Component Diagram](assets/component_diagram.png)

#### `util` and `exceptions`
For tale we created our own `ApplicationException` which `extends RuntimeException`
in order to simplify exception handling.

The globally used `*Util` classes are probably a sign of some bad design.
There are a total of 4 utilities, each of which should be self explanatory
given the comments and code.

#### `command` and picocli
We used a library called [picocli](https://picocli.info/) to build
the command line interface.
The `command` package contains all the commands which use annotations to
tell picocli about the parameters and command name.
When called, the `*Command` class delegates to the corresponding implementation.

#### `implementation`
As the name suggests, the `implementation` package contains the implementation
for every command. Here we use our home-built dependency injection
to get instances of the required services from the `repository` package.

#### `di` (Dependency Injection)
The `di` package contains a `ServiceFactory` which is reponsible for providing
objects with instances of the services they require.

#### `config`
Tale has a config file in the [TOML](https://en.wikipedia.org/wiki/TOML) format.
We currently use it to configure the commiter name and the date format.

#### `patch`
The `patch` package contains our core `DifferenceAlgorithm`, all
the different objects used to represent patches
(PatchLines, Hunks, HunkHeaders, Patches),
and the builders/helpers to generate and apply them.

\newpage

#### `serialization`
In order to serialize our objects to the filesystem, we used base64 encoded
JSON.
Google's [GSON](https://github.com/google/gson) library is used for
serialization/deserialization.
This package contains the custom serializers for the patch information.

#### `datastructures`
We built three custom datastructures for this project:

- a `Vertex{int x, int y}`
- a `Pair<T, U>{T left, U right}`
- a `SizeLimitedCache<K, V>`, which is just a size-limited `ConcurrentHashMap`

# Lessons Learned

## Planning

### Time allocation and estimation
The main lesson we learned with regards to planning is to account for more
time to solve potential problems.
We had to cut some corners to still fit in the
minimal feature set we wanted for tale.

### User story breakdown
We had some trouble with stories depending too much on the previous
task being done.
In additon the stories were too large,
and not well suited for parallelizing the workflow.

We found it to be very easy for the work to get chaotic, because
we would realize half-way through a story that a requirement was not met yet,
which encourages going back and forth,
and having many in-progress tasks at once.

\newpage

## Domain Object Model
The original plan for modeling our data and keeping it on disk can be found in
the requirement specification document.

During development we stuck to our original plan,
but in hindsight the model that git uses is far simpler and superior.

For each commit we store a list of `Patch`es,
which in turn consist of the file affected by the patch
as well as a list of `Hunk`s.
A `Hunk` is represents a set of different lines and the surrounding context.
It consists of a Hunk Header starting with `@@` and contains, in order,
the following elements:

1) the position of the hunk in the previous version
2) the length of the hunk in the previous version
3) the position of the hunk in the current version
4) the length of the hunk in the current version

Lines that were added are prefixed with '`+`',
lines that were removed are prefixed with '`-`',
and lines that remain the same are prefixed with a space character.

This patch format is called "Unified Format" and an example from the
[GNU diffutils manual](https://www.gnu.org/software/diffutils/manual/html_node/Example-Unified.html#Example-Unified) can be seen below:

```{.patch}
@@ -1,7 +1,6 @@
-The Way that can be told of is not the eternal Way;
-The name that can be named is not the eternal name.
 The Nameless is the origin of Heaven and Earth;
-The Named is the mother of all things.
+The named is the mother of all things.
+
 Therefore let there always be non-being,
   so we may see their subtlety,
 And let there always be being,
```
Git on the other hand stores a snapshot of the entire repository for each commit
and only generates patches when necessary (E.g. to show them to the user).

Git also has a much more flexible, recursive `tree` model to remember the
location of a file. We rely only on the name contained in the patch.
Our approach is error prone and not very efficient.

\newpage

## Patch Generation
\label{patchgeneration}
We attempted to design our own difference detection algorithm
and managed to cover about 70% of the cases in our test-suite,
but because this would be the foundation for tale, we decided to build our algorithm based on
["An O(ND) Difference Algorithm and Its Variations"](https://doi.org/10.1007/BF01840446)
by Eugene W. Myers.

In this chapter we will explain how we go from having an old (aka. *from*)
and new (aka. *to*) state to a `Patch` explained in a previous chapter.

*The picture on the next page is referenced several times in this explanation.
It may be helpful to look at the two side-by-side.*

For this example we will assume our inputs to be `from=[A,B,C,A,B,B,A]`
and `to=[C,B,A,B,A,C]`.
In tale, the inputs are `List<String>` containing lines from the respective files.

If we arrange the inputs like seen in the picture, with *from* along the top
and *to* along the left side, we can build what's called an "edit **graph**".
Any path in this edit graph from the top left to the bottom right is
a valid "edit **path**".
An edit path describes the steps to transform *from* into *to*.
*X- respectively Y-Positions reference the position of the
node that the edge points to*.

The interpretation of the edges an edit path follows is as follows:  
**Diagonal Edges** can be interpreted as a "match point" where no change is needed.  
**Horizontal Edges** can be interpreted as a deletion from *from*,
at the X-position.  
**Vertical Edges** can be interpreted as an insertion of the element
from *to* at the Y-position into *from* at the X-position.  

While the edit path from top-left via bottom-left to bottom-right
is technically valid, it's horrendously inefficient,
because it essentially says the the necessary transformation is to
add the entire new content (*to*), then delete the entire old content (*from*).

Thus what we want for an efficient (and readable) diff is the path with
the longest sequence of diagonals (match points).
The algorithm discussed in the Myers paper is elegant and fast.
We recommend reading the original paper for a detailed understanding.

In tale the `DifferenceAlgorithm` class creates a list of vertices (aka. nodes),
which the `PatchGenerator` turns into `Patch`es.

On the next page the most important snippet of the `PatchGenerator` is displayed.
The resulting `diff` list is then split into hunks by leaving 3 lines of context
(match points) on either side of the changed lines.

\newpage

![The edit graph from the original paper(from="ABCABBA", to="CBABAC").](assets/edit_graph.png){width=70%}

```{.java}
LinkedList<PatchLine> diff = new LinkedList<>();

for (int i = 1; i < editPath.size(); i++) {
    // two vertices define a diagonal
    Vertex curr = editPath.get(i - 1);
    Vertex next = editPath.get(i);

    // a PatchLine contains `curr` to build the HunkHeaders later
    if (isInsert(curr, next)) {
        diff.addFirst(new PatchLine(curr, "+" + to.get(curr.y - 1)));
    } else if (isDelete(curr, next)) {
        diff.addFirst(new PatchLine(curr, "-" + from.get(curr.x - 1)));
    } else {
        diff.addFirst(new PatchLine(curr, " " + from.get(curr.x - 1), true));
    }
}
return diff;
```

\newpage

## Testing
During the development of tale we did a lot of manual testing and debugging.
However most of the tests are JUnit tests.
We made extensive use of the
[CI functionality](https://docs.gitlab.com/ee/ci/README.html) of GitLab to:

- run our automated tests and
- check code coverage
- build the executable binaries

In all we achieved a code coverage of 56%, and have covered the most important
parts of the core functionality.

### Ease of testing
We realized too late that our Command-Structure would be hard to test,
and wasted quite some time trying to built automated integration tests.
In the end we tried to modularize them as best as possible and use
traditional unit tests.

## GraalVM Native Images
\label{nativeimages}
[GraalVM](https://www.graalvm.org/) is an alternative JVM/JDK based
on HotSpot/OpenJDK.
It supports polyglot programs (programs written in more than one language)
in languages such as Python, Java, Kotlin, JavaScript, and LLVM languages.

Tale is written in Java which has famously slow startup times.
To reduce this issue we chose to use GraalVM Native Images (henceforth NI) instead
of running with the conventional HotSpot JVM.
When using NI, Graal compiles the source code ahead-of-time and runs
a lot of optimizations (compilation times for tale are around 30-40 seconds),
then packages a minimal JVM called SubstrateVM into the binary.
The tale binary ends up being about 15MB in size,
and runs without any dependencies on most Linux systems.

The build process is fairly simple thanks to maven,
with the relevant plugin shown on the next page.

One of the complications we encountered when using NI is the limited reflection
support due to the ahead-of-time compilation.
We use reflection in tale for:

- JSON de/serialization with GSON
- command line interface with PicoCLI
- Dependency Injection (home made)

Graal NI Builder accepts a `reflect-config.json`,
where the desired reflection features per class must be listed.
PicoCLI generates the config it needs out of the box,
but we had to write a python script to generate the config for
GSON and our home made dependency injection.

\newpage

```{.xml}
<plugin>
    <groupId>org.graalvm.nativeimage</groupId>
    <artifactId>native-image-maven-plugin</artifactId>
    <version>19.3.0</version>

    <executions>
        <execution>
            <goals>
                <goal>native-image</goal>
            </goals>
            <phase>package</phase>
        </execution>
    </executions>
    <configuration>
        <mainClass>dev.tale.cli.Application</mainClass>
        <imageName>tale</imageName>
        <buildArgs>
-H:ReflectionConfigurationFiles=target/META-INF/native-image/.../reflect-config.json
        </buildArgs>
    </configuration>
</plugin>
```

# Version Control
The version control is located in the
[tale-doc Repository](https://gitlab.com/severinkaderli/tale) on GitLab.

The version history of this document can be read with the following command:

`git log -- documents/final-report.md`
