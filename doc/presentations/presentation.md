---
title: "tale"
subtitle: "an experimental Version Control System"
author:
    - Marius Schär
    - Severin Kaderli
lecturer:
    - Wolfgang Kaltz
lang: "en"
toc: true
main-color: 009688
rule-color: 009688 
link-color: 448AFF 
glossary: true
glossary-file: glossary.tex
links-as-notes: true
...

# Introduction

## Version Control Systems
\colsbegin
\col{50}

- Git
- Mercurial
- Subversion
- BitKeeper
- ClearCase
- $\hdots$


\col{50}

. . .

- [Many different approaches](https://en.wikipedia.org/wiki/Comparison_of_version-control_software)
  * Centralized vs. Decentralized
  * Snapshot vs. Changeset
  * Hashes vs. Numbers
  * Files vs. Trees
  * Lock vs. Merge

\colsend

## Goal

- Self Hosting ^[Version the code to tale with tale]
- Further our understanding of VCS

# Scope

## Scope

#### In Scope
- Build our own VCS
  * Learn what goes into a VCS
  * Improve our own knowledge
- Functionality inspired by git

#### Out of Scope
- Build something git compatible
- Build an exact replica of git

# Requirements

## Requirements
- shown in the demo
- we did not implemented branching and merging

## Requirements
#### show differences
- `tale diff`
- `tale diff --no-index <file> <file>`

. . .

#### record history
- `tale add <files>`
- `tale commit -m "<message>"`

## Requirements
#### show history
- `tale show <commitID>`
- `tale log`

. . .

#### undo history
- `tale revert <commitID>`

# Technical Details

## Runtime
- tale is written in Java
- GraalVM Native Images

#### GraalVM
- alternative to OpenJDK/HotSpotVM/C2
- Polyglot VM ^[Can run programs written in more than 1 language]

#### Native Image
- ahead of time compilation
- packaged together with SubstrateVM
- runs without dependencies

## Runtime
#### Pros
- starts up way faster than "normal" Java
- requires no JDK/JRE to be installed

. . .

#### Cons
- Garbage Collector isn't as good
- Some Java features are only partially supported

## Diffing - Sourcing
- Attempted building our own
  * _"moderate"_ success.
- Found paper by Eugene W. Myers 
  * ["An O(ND) Difference Algorithm and Its Variations (1986)"](https://doi.org/10.1007/BF01840446)

## Diffing - EditScript
- A way of transforming one list into another efficently
- In tale these are the Lines in two versions of a file

$$
\begin{aligned}
  from &= \text{AB}\textcolor{red}{CAB}\text{B}\textcolor{red}{A} \\
  to &= \textcolor{red}{C}\text{B}\textcolor{red}{ABA}\text{C} \\
  common &= \textcolor{red}{CABA}
\end{aligned}
$$

## Diffing - EditScript

- Deletion: `xD`
- Insertion: `xIy`

. . .

- Example: 1IC, 1D

```{.java}
String[] from = {A, B, C}
                {A, C, B, C}
String[] to   = {C, B, C}
```

## Diffing - EditGraph
\colsbegin
\col{60}

![](assets/grid.png)

\col{40}

- Edge arrival vertex $(x, y)$
  * $x$ indexes $from$
  * $y$ indexes $to$
- Any path from (0, 0) to (m, n) is valid


$$
\begin{aligned}
  \rightarrow &= \text{Deletion} \\
  \downarrow &= \text{Insertion} \\
\end{aligned}
$$

\colsend

## Diffing - EditGraph
\colsbegin
\col{60}

![](assets/long_path.png)

\col{30}

. . .

EditScript:  
1D, 2D, 3D, 4D, 5D, 6D, 7D  
1IC, 2IB, 3IA, 4IB, 5IA, 6IC

\col{10}

. . .

```{.diff}
-A
-B
-C
-A
-B
-B
-A
+C
+B
+A
+B
+A
+C

```

\colsend

## Diffing - EditGraph
\colsbegin
\col{60}

![](assets/grid.png)

\col{40}

- Some edits don't matter
  * 4D, 4IA

\colsend

## Diffing - EditGraph
\colsbegin
\col{60}

![](assets/full.png)

\col{40}

- shortest path problem
- longer sequences of diagonals are better
- any path is valid

\colsend

## Diffing - EditGraph
\colsbegin
\col{60}

![](assets/path.png)

\col{40}

```{.diff}
-A
-B
 C
+B
 A
 B
-B
 A
+C

```

\colsend

## Persistence
\colsbegin
\col{60}

![](assets/model_1.png)

\col{40}

```{.diff}
@@ -1,5 +1,6 @@
 # tale
-The VCS of the future
+An experimental VCS
+(not for production use)
 
 ## How to build
 `mvn clean install`

```

\colsend

## Persistence
\colsbegin
\col{60}

![](assets/model_2.png)

\col{40}

- lazily decoded
- often the patches aren't needed

\colsend

## Persistence
\colsbegin
\col{60}

![](assets/model_full.png)

\col{40}

- on disk as base64 encoded JSON
- `content` encoded seperately

\colsend

## Persistence
- different from e.g the git model
  * snapshot at every commit (git)
  * vs. just change sets (tale)
- leads to some problems

## History
- every `TaleObject` has an ID
  * SHA-256 digest of the  
    object content on disk
- graph where each commit points to parent
- model is set up for merging (`parent2ID`)

# Technical Problems
## Data Model
* Complex
* Actions need many steps to work
* Applying change sets in the future can be difficult

## Reflection
* GraalVM needs to know about Reflection
* Picocli provides reflection-config.json
* Python script for generating configuration

## Testability
* Difficult to automatically test commands
* Might be because of our design

## Stability
* Changes had big impact
* More automated tests might have solved this

# Process Problems
## Work Package Size
* Work packages were too big
* Dependencies were difficult to fulfill
* Splitting the work packages was difficult

## Poor Estimation
* Work packages took longer than excepted
* Be more lenient with estimation in the future

## Links

\LARGE gitlab.com/severinkaderli/tale

# Demo
